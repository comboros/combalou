const path = require('path');
const merge = require('webpack-merge');

const common = require('./webpack.common.js');

module.exports = merge(common, {
  mode: 'development',
  entry: {
    dev_server: 'webpack-dev-server/client?http://localhost:8080',
    main: path.resolve(__dirname, 'src', 'static', 'index.js'),
    admin: path.resolve(__dirname, 'src', 'static', 'admin.js'),
  },
  devServer: {
    // serve index.html in place of 404 responses
    historyApiFallback: true,
    contentBase: './src',
    hot: true,
    proxy: {
      '/api/**': {
        target: 'http://[::1]:8000',
        pathRewrite: {'^/api': ''},
        secure: false,
      }
    }
  },
  module: {
    rules: [{
      test: /\.elm$/,
      include: [
        path.resolve(__dirname, 'src/Form')
      ],
      use: {
        loader: 'elm-webpack-loader',
        options: {
          cwd: path.resolve(__dirname),
          verbose: true,
          debug: true
        }
      }
    }, {
      test: /\.elm$/,
      include: [
        path.resolve(__dirname, 'src/Admin')
      ],
      use: {
        loader: 'elm-webpack-loader',
        options: {
          cwd: path.resolve(__dirname),
          verbose: true,
          debug: true
        }
      }
    }, {
      test: /\.sc?ss$/,
      use: ['style-loader', 'css-loader', 'sass-loader']
    }]
  }
});

