const autoprefixer = require('autoprefixer');
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  output: {
    filename: '[name]-[hash].js',
    path: path.resolve(__dirname, 'dist'),
  },
  resolve: {
    extensions: ['.js', '.elm'],
    modules: ['node_modules'],
  },
  module: {
    noParse: /\.elm$/,
    rules: [{
      test: /\.(eot|ttf|woff|woff2|svg)$/,
      use: 'file-loader?publicPath=../../&name=static/css/[hash].[ext]',
    }]
  },
  plugins: [
    new webpack.LoaderOptionsPlugin({
      options: {
        postcss: [autoprefixer()],
      }
    }),
    new HtmlWebpackPlugin({
      chunks: ['main'],
      template: path.resolve(__dirname, 'src/static/index.html'),
      inject: 'body',
      filename: 'index.html',
    }),
    new HtmlWebpackPlugin({
      chunks: ['admin'],
      template: path.resolve(__dirname, 'src/static/admin.html'),
      inject: 'body',
      filename: 'admin.html'
    })
  ]
}

