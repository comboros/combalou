<?php

require_once 'inc/config.php';
require_once 'inc/mailing-service.php';
require_once 'inc/repository.php';

// get registration

$json = file_get_contents('php://input');
$obj = json_decode($json);

// validate

if (!$obj) {
    error_log('Received invalid JSON');
    http_response_code(400);
    echo 'Invalid JSON';
    exit();
}

try {
    $registration = Registration::fromJsonObject($obj);
} catch (Exception $e) {
    error_log($e->getMessage());
    http_response_code(400);
    echo 'Invalid registration:' . $e->getMessage();
    exit();
}

// db

$mysqli = mysqli_connect(Config::db_url, Config::db_user, Config::db_passwd, Config::db_name);
if (mysqli_connect_errno($mysqli)) {
    http_response_code(500);
    echo 'Échec lors de la connexion à MySQL : ' . mysqli_connect_error();
    exit();
}
if (!$mysqli->set_charset(Config::db_charset)) {
    http_response_code(500);
    echo "Échec lors de la définition de l’encodage de la connexion : $mysqli->error";
    exit();
}

// send mail
$ticketRepository = new TicketRepository($mysqli);
$workshopRepository = new WorkshopRepository($mysqli);
$service = new MailingService($ticketRepository, $workshopRepository);
$ret = $service->sendConfirmation($registration, 13);
if ($ret) {
    http_response_code(200);
} else {
    http_response_code(500);
}
