module Admin.Data.Registration exposing (Address, PaymentType(..), PersonalInfo, PriceType(..), Registration, RegistrationState(..), decoder, delete, fetch, sendInvoice, suggest, updateBasket, updateBooking, updatePersonalInfo, validate)

import Admin.Data.Basket exposing (Ticket)
import Common.Basket exposing (Basket(..))
import Common.Url as Url
import Common.Workshop as Workshop exposing (Workshop)
import Http exposing (Request)
import Json.Decode as Decode exposing (Decoder, andThen, fail, int, list, maybe, string, succeed)
import Json.Decode.Pipeline exposing (custom, optional, required)
import Json.Encode as Encode
import Time


type alias Registration =
    { id : Int
    , name : String
    , surname : String
    , state : RegistrationState
    , paymentType : PaymentType
    , priceType : PriceType
    , address : Address
    , email : String
    , phone : String
    , responsible : Maybe String
    , basket : List Ticket
    , workshops : List Workshop
    , lastBilling : Time.Posix
    }


type alias Address =
    { line1 : String
    , line2 : Maybe String
    , postalCode : String
    , city : String
    , country : String
    }


type RegistrationState
    = Unpaid
    | Paid
    | Inconsistent


type PaymentType
    = Check
    | Transfer


type PriceType
    = Reduced
    | Full



{- Partial datatypes used to update part of a registration -}


type alias PersonalInfo =
    { name : String
    , surname : String
    , responsible : Maybe String
    , addressLine1 : String
    , addressLine2 : Maybe String
    , postalCode : String
    , city : String
    , country : String
    , phone : String
    , email : String
    , priceType : PriceType
    , paymentType : PaymentType
    , state : RegistrationState
    }



{- ENCODE/DECODE -}


decoder : Decoder Registration
decoder =
    succeed Registration
        |> required "id" int
        |> required "name" string
        |> required "surname" string
        |> required "state" registrationStateDecoder
        |> required "paymentType" paymentTypeDecoder
        |> required "priceType" priceTypeDecoder
        |> custom addressDecoder
        |> required "email" string
        |> required "phone" string
        |> optional "responsible" (maybe string) Nothing
        |> optional "basket" (list Admin.Data.Basket.decoder) []
        |> optional "workshops" (list Workshop.decoder) []
        |> required "lastBilling"
            (int
                |> Decode.map (\x -> x * 1000)
                -- Backend returns timestamps in seconds
                |> Decode.map Time.millisToPosix
            )


addressDecoder : Decoder Address
addressDecoder =
    succeed Address
        |> required "line1" string
        |> required "line2" (maybe string)
        |> required "postalCode" string
        |> required "city" string
        |> required "country" string


registrationStateDecoder : Decoder RegistrationState
registrationStateDecoder =
    string |> andThen stateFromString


stateFromString : String -> Decoder RegistrationState
stateFromString str =
    case str of
        "paid" ->
            succeed Paid

        "unpaid" ->
            succeed Unpaid

        "inconsistent" ->
            succeed Inconsistent

        _ ->
            fail <| "Invalid registration state: " ++ str


paymentTypeDecoder : Decoder PaymentType
paymentTypeDecoder =
    string |> andThen paymentTypeFromString


paymentTypeFromString : String -> Decoder PaymentType
paymentTypeFromString str =
    case str of
        "check" ->
            succeed Check

        "transfer" ->
            succeed Transfer

        _ ->
            fail <| "Invalid payment type: " ++ str


priceTypeDecoder : Decoder PriceType
priceTypeDecoder =
    string |> andThen priceTypeFromString


priceTypeFromString : String -> Decoder PriceType
priceTypeFromString str =
    case str of
        "full" ->
            succeed Full

        "reduced" ->
            succeed Reduced

        _ ->
            fail <| "Invalid price type: " ++ str



{- HTTP -}


fetch : Int -> Request Registration
fetch id =
    Http.get (Url.registration id) decoder


suggest : String -> Request (List Registration)
suggest term =
    Http.get (Url.suggest term) (list decoder)


validate : Int -> Request String
validate id =
    let
        url =
            Url.payment id
    in
    Http.request
        { method = "POST"
        , headers = []
        , url = url
        , body = Http.emptyBody
        , expect = Http.expectString
        , timeout = Nothing
        , withCredentials = False
        }


encodePersonalInfo : PersonalInfo -> Encode.Value
encodePersonalInfo info =
    Encode.object
        [ ( "surname", Encode.string info.surname )
        , ( "name", Encode.string info.name )
        , ( "responsible", Maybe.map Encode.string info.responsible |> Maybe.withDefault Encode.null )
        , ( "addressLine1", Encode.string info.addressLine1 )
        , ( "addressLine2", Maybe.map Encode.string info.addressLine2 |> Maybe.withDefault Encode.null )
        , ( "postalCode", Encode.string info.postalCode )
        , ( "city", Encode.string info.city )
        , ( "country", Encode.string info.country )
        , ( "phone", Encode.string info.phone )
        , ( "email", Encode.string info.email )
        , ( "priceType", priceTypeEncoder info.priceType )
        , ( "paymentType", paymentTypeEncoder info.paymentType )
        , ( "state", registrationStateEncoder info.state )
        ]


priceTypeEncoder : PriceType -> Encode.Value
priceTypeEncoder priceType =
    case priceType of
        Reduced ->
            Encode.string "reduced"

        Full ->
            Encode.string "full"


paymentTypeEncoder : PaymentType -> Encode.Value
paymentTypeEncoder paymentType =
    case paymentType of
        Check ->
            Encode.string "check"

        Transfer ->
            Encode.string "transfer"


registrationStateEncoder : RegistrationState -> Encode.Value
registrationStateEncoder state =
    case state of
        Unpaid ->
            Encode.string "unpaid"

        Paid ->
            Encode.string "paid"

        Inconsistent ->
            Encode.string "inconsistent"


updatePersonalInfo : Int -> PersonalInfo -> Request String
updatePersonalInfo id info =
    let
        url =
            Url.visitor id

        body =
            encodePersonalInfo info
                |> Http.jsonBody
    in
    Http.request
        { method = "PUT"
        , headers = []
        , url = url
        , body = body
        , expect = Http.expectString
        , timeout = Nothing
        , withCredentials = False
        }


updateBooking : Int -> List Int -> Request String
updateBooking id workshopIds =
    let
        url =
            Url.bookings id

        body =
            Encode.list Encode.int workshopIds
                |> Http.jsonBody
    in
    Http.request
        { method = "PUT"
        , headers = []
        , url = url
        , body = body
        , expect = Http.expectString
        , timeout = Nothing
        , withCredentials = False
        }


updateBasket : Int -> Common.Basket.Basket -> Request String
updateBasket id (Basket ( _, bundle ) unitaries) =
    let
        url =
            Url.basket id

        encode ( t, q ) =
            Encode.object
                [ ( "ticket", Encode.int t.id )
                , ( "quantity", Encode.int q )
                ]

        body =
            ( bundle, 1 )
                :: unitaries
                |> List.filter (\( t, q ) -> q > 0)
                |> Encode.list encode
                |> Http.jsonBody
    in
    Http.request
        { method = "PUT"
        , headers = []
        , url = url
        , body = body
        , expect = Http.expectString
        , timeout = Nothing
        , withCredentials = False
        }


delete : Int -> Request String
delete id =
    let
        url =
            Url.registration id
    in
    Http.request
        { method = "DELETE"
        , headers = []
        , url = url
        , body = Http.emptyBody
        , expect = Http.expectString
        , timeout = Nothing
        , withCredentials = False
        }


sendInvoice : Int -> Request String
sendInvoice id =
    let
        url =
            Url.invoice id
    in
    Http.request
        { method = "POST"
        , headers = []
        , url = url
        , body = Http.emptyBody
        , expect = Http.expectString
        , timeout = Nothing
        , withCredentials = False
        }
