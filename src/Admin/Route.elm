module Admin.Route exposing (Route(..), fromUrl, href, replaceUrl)

import Browser.Navigation as Navigation
import Html exposing (Attribute)
import Html.Attributes
import Url exposing (Url)
import Url.Parser exposing ((</>), Parser, int, map, oneOf, s)


type Route
    = Home
    | Detail Int
    | UpdateInfo Int
    | UpdateWorkshops Int
    | UpdateBasket Int


routeParser : Parser (Route -> a) a
routeParser =
    oneOf
        [ map Home (s "")
        , map Detail (s "registration" </> int)
        , map UpdateInfo (s "registration" </> int </> s "personal-info" </> s "update")
        , map UpdateWorkshops (s "registration" </> int </> s "workshops" </> s "update")
        , map UpdateBasket (s "registration" </> int </> s "basket" </> s "update")
        ]


routeToString : Route -> String
routeToString route =
    let
        path =
            case route of
                Home ->
                    [ "" ]

                Detail id ->
                    [ "registration", String.fromInt id ]

                UpdateInfo id ->
                    [ "registration", String.fromInt id, "personal-info", "update" ]

                UpdateWorkshops id ->
                    [ "registration", String.fromInt id, "workshops", "update" ]

                UpdateBasket id ->
                    [ "registration", String.fromInt id, "basket", "update" ]
    in
    "#/" ++ String.join "/" path


href : Route -> Attribute msg
href route =
    Html.Attributes.href (routeToString route)


fromUrl : Url -> Maybe Route
fromUrl url =
    -- Treat fragment as if it was the path
    { url | path = Maybe.withDefault "" url.fragment, fragment = Nothing }
        |> Url.Parser.parse routeParser


replaceUrl : Navigation.Key -> Route -> Cmd msg
replaceUrl key route =
    Navigation.replaceUrl key (routeToString route)
