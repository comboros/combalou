module Admin.Pages.Home exposing (Model, Msg, init, update, view)

import Accessibility exposing (Html, a, article, button, div, inputText, li, p, section, small, strong, text, ul)
import Accessibility.Style exposing (invisible)
import Admin.Data.Registration exposing (Registration)
import Admin.Route as Route
import Common.View exposing (spinner, viewFetchError, viewRegistrationState)
import Html exposing (Attribute)
import Html.Attributes exposing (class, id, placeholder, type_)
import Html.Events exposing (onInput, onSubmit)
import Http



{- MODEL -}


type alias Model =
    { term : String
    , results : Results
    }


type Results
    = Searching
    | Loaded (List Registration)
    | SearchFailed Http.Error


init : ( Model, Cmd Msg )
init =
    ( { term = "", results = Loaded [] }, Cmd.none )



{- VIEW -}


view : Model -> { title : String, body : Html Msg }
view model =
    { title = "Accueil"
    , body =
        section [ class "section" ]
            [ article [ class "level" ]
                [ searchForm [ class "level-item" ]
                    [ div [ class "field has-addons" ]
                        [ searchLabel "N° d’inscription ou nom"
                        , searchInput "search-input"
                            model.term
                            [ placeholder "1234 ou Jean Valjean"
                            , onInput EnteredTerm
                            ]
                        , searchButton "Rechercher"
                        ]
                    ]
                ]
            , article [ class "columns is-centered is-mobile" ]
                [ searchResults model.results ]
            ]
    }


searchLabel : String -> Html Msg
searchLabel labelContent =
    Html.label invisible [ text labelContent ]


searchInput : String -> String -> List (Attribute Msg) -> Html Msg
searchInput name value attrs =
    div [ class "control" ]
        [ inputText value ([ id name, class "input is-medium is-expanded" ] ++ attrs) ]


searchButton : String -> Html Msg
searchButton label =
    div [ class "control" ]
        [ button [ type_ "submit", class "button is-info is-medium" ]
            [ text label ]
        ]


searchForm : List (Attribute Msg) -> List (Html Msg) -> Html Msg
searchForm attrs content =
    Html.form (onSubmit SubmittedForm :: attrs)
        content


searchResults : Results -> Html Msg
searchResults results =
    case results of
        Searching ->
            spinner

        Loaded registrations ->
            ul [] <|
                List.map viewResultEntry registrations

        SearchFailed e ->
            viewFetchError e


viewResultEntry : Registration -> Html Msg
viewResultEntry registration =
    let
        name =
            registration.surname ++ " " ++ registration.name

        id =
            "#" ++ String.fromInt registration.id
    in
    li [ class "registration-suggestion" ]
        [ a [ Route.href (Route.Detail registration.id) ]
            [ strong [] [ text name ]
            , text " "
            , small [ class "has-text-grey" ] [ text id ]
            , text " "
            , viewRegistrationState registration.state
            ]
        ]



{- UPDATE -}


type Msg
    = EnteredTerm String
    | SubmittedForm
    | CompletedSearch (Result Http.Error (List Registration))


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        EnteredTerm term ->
            ( { model | term = term }, Cmd.none )

        SubmittedForm ->
            ( { model | results = Searching }
            , Admin.Data.Registration.suggest model.term
                |> Http.send CompletedSearch
            )

        CompletedSearch (Ok results) ->
            ( { model | results = Loaded results }, Cmd.none )

        CompletedSearch (Err error) ->
            ( { model | results = SearchFailed error }, Cmd.none )
