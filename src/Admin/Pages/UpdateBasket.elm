module Admin.Pages.UpdateBasket exposing (Model, Msg, init, update, view)

import Accessibility exposing (Html, a, article, button, div, fieldset, label, legend, p, section, text)
import Admin.Data.Registration as Registration exposing (Registration)
import Admin.Route as Route
import Browser.Navigation as Navigation
import Common.Basket as Basket exposing (Basket(..), BundleChoice, UnitaryChoice)
import Common.Ticket as Ticket exposing (Ticket, TicketDTO)
import Common.View exposing (spinner, viewFetchError)
import Html exposing (input)
import Html.Attributes exposing (checked, class, for, id, min, name, type_, value)
import Html.Events exposing (onInput, onSubmit)
import Http
import Json.Decode as Json



{- MODEL -}


type alias Model =
    { id : Int
    , key : Navigation.Key
    , status : Status
    }


type Status
    = Loading
    | LoadingGotCatalog (List TicketDTO)
    | LoadingGotRegistration Registration
    | LoadingFailed Http.Error
    | BasketConstructionFailed String
    | Editing Basket (Maybe String)
    | Saving Basket
    | SavingFailed Basket Http.Error


init : Int -> Navigation.Key -> ( Model, Cmd Msg )
init id key =
    let
        registration =
            Registration.fetch id
                |> Http.send CompletedRegistrationLoad

        catalog =
            Ticket.fetchAll
                |> Http.send CompletedCatalogLoad
    in
    ( { id = id, key = key, status = Loading }
    , Cmd.batch [ registration, catalog ]
    )



{- VIEW -}


view : Model -> { title : String, body : Html Msg }
view model =
    case model.status of
        LoadingFailed error ->
            { title = "Problème lors du chargement"
            , body =
                section [ class "section" ]
                    [ viewFetchError error
                    , viewBackButton model.id
                    ]
            }

        BasketConstructionFailed error ->
            { title = "Problème lors du chargement"
            , body =
                section [ class "section" ]
                    [ article [ class "message is-danger" ]
                        [ div [ class "message header" ]
                            [ p []
                                [ text "Je n’ai pas pu récupérer le panier ! ☹" ]
                            ]
                        , div [ class "message-body content" ]
                            [ text error ]
                        ]
                    , viewBackButton model.id
                    ]
            }

        Editing (Basket bundleChoice unitaryChoice) maybeError ->
            let
                notification =
                    Maybe.map
                        (\error -> [ article [ class "notification is-danger" ] [ text error ] ])
                        maybeError
            in
            { title = "Édition du panier de l’inscription #" ++ String.fromInt model.id
            , body =
                section [ class "section" ]
                    [ Html.form [ name "basket", onSubmit SubmittedForm ] <|
                        Maybe.withDefault [] notification
                            ++ [ viewBundles bundleChoice ]
                            ++ viewUnitaries unitaryChoice
                            ++ [ div [ class "field" ]
                                    [ viewBackButton model.id
                                    , viewSubmitButton
                                    ]
                               ]
                    ]
            }

        SavingFailed _ error ->
            { title = "Problème lors de la sauvegarde des modifications"
            , body =
                section [ class "section" ]
                    [ viewFetchError error
                    , viewBackButton model.id
                    ]
            }

        _ ->
            { title = "Chargement"
            , body =
                section [ class "section " ]
                    [ spinner
                    , viewBackButton model.id
                    ]
            }


viewBundles : BundleChoice -> Html Msg
viewBundles ( tickets, selection ) =
    fieldset [ class "field" ] <|
        [ legend [ class "label" ] [ text "Forfait" ] ]
            ++ List.map
                (\t ->
                    label [ class "radio" ]
                        [ input
                            [ type_ "radio"
                            , name "radio-bundles"
                            , checked (t == selection)
                            , onInput (always (SelectedBundle t))
                            ]
                            []
                        , text t.name
                        ]
                )
                tickets


viewUnitaries : UnitaryChoice -> List (Html Msg)
viewUnitaries unitaries =
    List.map
        (\( t, q ) ->
            div [ class "field" ]
                [ label [ class "label", for ("input-number-" ++ String.fromInt t.id) ]
                    [ text t.name ]
                , div [ class "control" ]
                    [ input
                        [ type_ "number"
                        , id ("input-number-" ++ String.fromInt t.id)
                        , Html.Attributes.min "0"
                        , value (String.fromInt q)
                        , onInput (String.toInt >> ChangedQuantity t)
                        ]
                        []
                    ]
                ]
        )
        unitaries


viewBackButton : Int -> Html msg
viewBackButton id =
    a [ class "button is-light", Route.href (Route.Detail id) ]
        [ text "Retour" ]


viewSubmitButton : Html msg
viewSubmitButton =
    button [ type_ "submit", class "button is-info is-pulled-right" ]
        [ text "Valider" ]



{- UPDATE -}


type Msg
    = SelectedBundle Ticket
    | ChangedQuantity Ticket (Maybe Int)
    | SubmittedForm
    | CompletedRegistrationLoad (Result Http.Error Registration)
    | CompletedCatalogLoad (Result Http.Error (List TicketDTO))
    | CompletedSave (Result Http.Error String)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        CompletedRegistrationLoad (Ok registration) ->
            case model.status of
                Loading ->
                    ( { model | status = LoadingGotRegistration registration }, Cmd.none )

                LoadingGotCatalog catalog ->
                    case Basket.build registration.basket catalog of
                        Ok basket ->
                            ( { model | status = Editing basket Nothing }, Cmd.none )

                        Err error ->
                            ( { model | status = BasketConstructionFailed error }, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        CompletedRegistrationLoad (Err error) ->
            ( { model | status = LoadingFailed error }, Cmd.none )

        CompletedCatalogLoad (Ok catalog) ->
            case model.status of
                Loading ->
                    ( { model | status = LoadingGotCatalog catalog }, Cmd.none )

                LoadingGotRegistration registration ->
                    case Basket.build registration.basket catalog of
                        Ok basket ->
                            ( { model | status = Editing basket Nothing }, Cmd.none )

                        Err error ->
                            ( { model | status = BasketConstructionFailed error }, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        CompletedCatalogLoad (Err error) ->
            ( { model | status = LoadingFailed error }, Cmd.none )

        SelectedBundle ticket ->
            case model.status of
                Editing basket error ->
                    ( { model | status = Editing (Basket.selectBundle ticket basket) error }, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        ChangedQuantity ticket quantity ->
            case model.status of
                Editing basket error ->
                    ( { model | status = Editing (Basket.changeQuantity ticket (Maybe.withDefault 0 quantity) basket) error }, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        SubmittedForm ->
            case model.status of
                Editing basket _ ->
                    let
                        delta =
                            Basket.countWorkshops basket - Basket.maxWorkshops

                        error =
                            "Le nombre total d’ateliers dans le panier dépasse le nombre maximal d’ateliers pour cette édition ! "
                                ++ "Retirez-en au moins "
                                ++ String.fromInt delta
                                ++ " pour pouvoir valider ce nouveau panier."
                    in
                    if delta > 0 then
                        ( { model | status = Editing basket (Just error) }, Cmd.none )

                    else
                        ( { model | status = Saving basket }
                        , Registration.updateBasket model.id basket
                            |> Http.send CompletedSave
                        )

                _ ->
                    ( model, Cmd.none )

        CompletedSave (Ok _) ->
            ( model, Route.replaceUrl model.key (Route.Detail model.id) )

        CompletedSave (Err e) ->
            case model.status of
                Saving basket ->
                    ( { model | status = SavingFailed basket e }, Cmd.none )

                _ ->
                    ( model, Cmd.none )
