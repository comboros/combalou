module Common.Day exposing (Day(..), days, decoder, toString)

import Json.Decode as Decode exposing (Decoder, andThen, fail, string, succeed)


type Day
    = Saturday
    | Sunday
    | Monday
    | Tuesday
    | Wednesday
    | Thursday
    | Friday


days : List Day
days =
    [ Thursday, Friday, Saturday, Sunday ]


toString : Day -> String
toString day =
    case day of
        Saturday ->
            "Samedi"

        Sunday ->
            "Dimanche"

        Monday ->
            "Lundi"

        Tuesday ->
            "Mardi"

        Wednesday ->
            "Mercredi"

        Thursday ->
            "Jeudi"

        Friday ->
            "Vendredi"


decoder : Decoder Day
decoder =
    string
        |> andThen
            (\str ->
                case str of
                    "saturday" ->
                        succeed Saturday

                    "sunday" ->
                        succeed Sunday

                    "monday" ->
                        succeed Monday

                    "tuesday" ->
                        succeed Tuesday

                    "wednesday" ->
                        succeed Wednesday

                    "thursday" ->
                        succeed Thursday

                    "friday" ->
                        succeed Friday

                    _ ->
                        fail <| "Unknown day: " ++ str
            )
