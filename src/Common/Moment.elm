module Common.Moment exposing (Moment(..), decoder, moments, toString)

import Json.Decode exposing (Decoder, andThen, fail, string, succeed)


type Moment
    = Morning
    | Afternoon


moments : List Moment
moments =
    [ Morning, Afternoon ]


toString : Moment -> String
toString moment =
    case moment of
        Morning ->
            "Matin"

        Afternoon ->
            "Après-midi"


decoder : Decoder Moment
decoder =
    string
        |> andThen
            (\str ->
                case str of
                    "morning" ->
                        succeed Morning

                    "afternoon" ->
                        succeed Afternoon

                    _ ->
                        fail <| "Unknown moment: " ++ str
            )
