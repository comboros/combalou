module Common.Ticket exposing (PriceType, Ticket, TicketDTO, fetchAll, fromDTO)

import AssocList as Dict exposing (Dict)
import Common.Url as Url
import Http exposing (Request)
import Json.Decode as Decode exposing (Decoder, float, int, list, string)
import Json.Decode.Pipeline exposing (required)


type alias Ticket =
    { id : Int
    , name : String
    , description : String
    , workshops : Int
    , price : Dict PriceType Float
    }


type PriceType
    = Normal
    | Reduced


type alias TicketDTO =
    { id : Int
    , name : String
    , description : String
    , workshops : Int
    , price : Float
    , reducedPrice : Float
    , span : String
    }


dtoDecoder : Decoder TicketDTO
dtoDecoder =
    Decode.succeed TicketDTO
        |> required "id" int
        |> required "name" string
        |> required "description" string
        |> required "workshops" int
        |> required "price" float
        |> required "reducedPrice" float
        |> required "span" string


fromDTO : TicketDTO -> Ticket
fromDTO dto =
    { id = dto.id
    , name = dto.name
    , description = dto.description
    , workshops = dto.workshops
    , price = Dict.fromList [ ( Normal, dto.price ), ( Reduced, dto.reducedPrice ) ]
    }


fetchAll : Request (List TicketDTO)
fetchAll =
    Http.get Url.tickets (list dtoDecoder)
