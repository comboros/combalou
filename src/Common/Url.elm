module Common.Url exposing (basket, bookings, invoice, payment, registration, registrations, suggest, tickets, visitor, workshops)


prefix : String
prefix =
    "/api"


basket : Int -> String
basket id =
    prefix ++ "/visitor/" ++ String.fromInt id ++ "/basket"


bookings : Int -> String
bookings id =
    prefix ++ "/visitor/" ++ String.fromInt id ++ "/booking"


invoice : Int -> String
invoice id =
    prefix ++ "/visitor/" ++ String.fromInt id ++ "/invoice"


payment : Int -> String
payment id =
    prefix ++ "/visitor/" ++ String.fromInt id ++ "/payment"


registration : Int -> String
registration id =
    prefix ++ "/registration/" ++ String.fromInt id


registrations : String
registrations =
    prefix ++ "/registrations"


suggest : String -> String
suggest term =
    prefix ++ "/registrations?term=" ++ term


tickets : String
tickets =
    prefix ++ "/tickets"


visitor : Int -> String
visitor id =
    prefix ++ "/visitor/" ++ String.fromInt id


workshops : String
workshops =
    prefix ++ "/workshops"
