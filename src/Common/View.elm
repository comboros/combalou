module Common.View exposing (spinner, viewErrorExplanation, viewFetchError, viewInputText, viewRegistrationState, viewRetryLink)

import Accessibility exposing (Html, article, blockquote, button, div, inputText, label, p, span, text)
import Accessibility.Aria as Aria
import Accessibility.Style exposing (invisible)
import Accessibility.Widget exposing (invalid)
import Admin.Data.Registration exposing (RegistrationState(..))
import Html.Attributes exposing (class, for, id, style)
import Html.Events exposing (onClick, onInput)
import Http exposing (Error(..))


viewFetchError : Error -> Html msg
viewFetchError err =
    article [ class "message is-danger" ]
        [ div [ class "message-header" ]
            [ p []
                [ text "Je n’ai pas pu effectuer cette action ! ☹" ]
            ]
        , div [ class "message-body content" ] <|
            viewErrorExplanation err
        ]


viewErrorExplanation : Error -> List (Html msg)
viewErrorExplanation err =
    case err of
        BadUrl url ->
            [ p []
                [ text "L’URL spécifiée dans le formulaire est invalide. La voici :"
                , blockquote [] [ text url ]
                ]
            ]

        Timeout ->
            [ p []
                [ text "La requête a mis trop de temps à répondre. "
                , text "Peut-être que notre serveur est surchargé… pourriez-vous essayer à nouveau un peu plus tard ?"
                ]
            ]

        NetworkError ->
            [ p []
                [ text "Je ne parviens pas à accéder à internet."
                , text "Peut-être pourriez-vous vérifier votre connexion\u{00A0}?"
                ]
            ]

        BadStatus res ->
            [ p []
                [ text <| "Le serveur me répond un code d’erreur " ++ String.fromInt res.status.code ++ "." ]
            , p []
                [ text <| "Voici l’erreur qui m’est renvoyée :" ]
            , blockquote []
                [ text res.body ]
            ]

        BadPayload msg _ ->
            [ p []
                [ text "Je ne suis pas parvenu à décoder la réponse du serveur. " ]
            , p []
                [ text <| "Voici l’erreur qui m’est renvoyée :" ]
            , blockquote []
                [ text msg ]
            ]


viewRetryLink : Error -> msg -> Html msg
viewRetryLink err retry =
    if isErrorRecoverable err then
        p []
            [ text "Vous pouvez "
            , button [ onClick retry, class "button is-light is-small" ]
                [ text "réessayer" ]
            , text "."
            ]

    else
        p [] []


viewSupportEmail : Html msg
viewSupportEmail =
    p [ class "is-size-7" ]
        [ text "En cas de problème, il est toujours possible de contacter la commission web sur discord."
        ]


isErrorRecoverable : Error -> Bool
isErrorRecoverable err =
    case err of
        NetworkError ->
            True

        Timeout ->
            True

        BadStatus res ->
            let
                code =
                    res.status.code
            in
            if code == 512 then
                -- Bad Gateway
                True

            else
                False

        _ ->
            False


spinner : Html msg
spinner =
    div [ class "container" ]
        [ p invisible [ text "Chargement" ]
        , div [ class "spinner" ]
            [ div [ class "bouncer" ] []
            , div [ class "bouncer is-delayed" ] []
            ]
        ]


viewRegistrationState : RegistrationState -> Html msg
viewRegistrationState state =
    case state of
        Paid ->
            span [ class "tag is-medium is-success", style "margin-left" "1rem" ]
                [ text "Payée" ]

        Unpaid ->
            span [ class "tag is-medium is-warning", style "margin-left" "1rem" ]
                [ text "À régler" ]

        Inconsistent ->
            span [ class "tag is-medium is-danger", style "margin-left" "1rem" ]
                [ text "Incohérent" ]


type alias InputTextSpec a =
    { label : String
    , id : String
    , value : String
    , message : String -> a
    , error : Maybe String
    , help : Maybe String
    , mandatory : Bool
    }


viewInputText : InputTextSpec msg -> Html msg
viewInputText spec =
    div [ class "field" ] <|
        [ label
            [ class
                (if spec.mandatory then
                    "label mandatory"

                 else
                    "label"
                )
            , for spec.id
            ]
            [ text spec.label ]
        , div [ class "control" ]
            [ inputText
                spec.value
                [ class (inputClass spec.error)
                , onInput spec.message
                , id spec.id
                , invalid (spec.error /= Nothing)
                , Aria.errorMessage (spec.id ++ "-error")
                ]
            ]
        ]
            ++ (if spec.error /= Nothing then
                    [ p [ class "help is-danger", id (spec.id ++ "-error") ]
                        (Maybe.map (List.singleton << text) spec.error
                            |> Maybe.withDefault []
                        )
                    ]

                else
                    []
               )
            ++ (if spec.help /= Nothing then
                    [ p [ class "help", id (spec.id ++ "-help") ]
                        (Maybe.map (List.singleton << text) spec.help
                            |> Maybe.withDefault []
                        )
                    ]

                else
                    []
               )


inputClass : Maybe String -> String
inputClass =
    Maybe.map (always "input is-danger") >> Maybe.withDefault "input"
