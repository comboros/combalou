module Common.Category exposing (Category(..), decoder)

import Json.Decode exposing (Decoder, andThen, fail, string, succeed)


type Category
    = Instrument
    | Danse
    | Other


decoder : Decoder Category
decoder =
    string
        |> andThen
            (\str ->
                case str of
                    "danse" ->
                        succeed Danse

                    "instrument" ->
                        succeed Instrument

                    "autre" ->
                        succeed Other

                    _ ->
                        fail <| "Unknown category: " ++ str
            )
