<?php
/*
 * Saves the registration BEFORE redirecting to the bank site for
 * online payment.
 */
require_once 'inc/config.php';
require_once 'inc/model.php';
require_once 'inc/repository.php';
require_once 'inc/mailing-service.php';
require_once 'inc/utils.php';

// receive

$json = file_get_contents('php://input');
$obj = json_decode($json);

// validate

if (!$obj) {
    error_log('Received invalid JSON');
    http_response_code(400);
    echo 'Invalid JSON: ' . json_last_error_msg();
    exit();
}

try {
    $registration = Registration::fromJsonObject($obj);
} catch (Exception $e) {
    error_log($e->getMessage());
    http_response_code(400);
    echo 'Invalid registration:' . $e->getMessage();
    exit();
}

// persist

$mysqli = connect();

$repository = new RegistrationRepository($mysqli);
try {
    $id = $repository->save($registration);
} catch (Exception $e) {
    error_log($e->getMessage());
    http_response_code(500);
    echo $e->getMessage();
    exit();
}

header('Content-Type: application/json');
echo json_encode($id);

try {
    $ticketRepository = new TicketRepository($mysqli);
    $workshopRepository = new WorkshopRepository($mysqli);
    $mailingService = new MailingService($ticketRepository, $workshopRepository);
    $ret = $mailingService->sendConfirmation($registration, $id);
    if (!$ret) {
        error_log("Could not send an email to $registration->email ($id)");
    }
} catch (Exception $e) {
    error_log("Could not send an email to $registration->email ($id)");
    error_log($e->getMessage());
}

