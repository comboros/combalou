<?php
/**
 * GET
 *
 * Suggest registrations based on an arbitrary term.
 */

require_once 'inc/utils.php';
require_once 'inc/model.php';
require_once 'inc/repository.php';

if (!isset($_GET['term']) || !($term = $_GET['term'])) {
    err('term query parameter is mandatory', 400);
}

$mysqli = connect();
$registrationRepository = new RegistrationRepository($mysqli);
$registrations = $registrationRepository->suggest($term);

header('Content-Type: application/json');
echo json_encode($registrations);
