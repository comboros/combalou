This is the backend for Comboros’s registration form

It is very crude, written in vanilla Php because it was needed ASAP on a very simple server that did not include fancy technologies.

# Resources

# `all-registrations-csv.php`
**Method**: `GET`.
**Parameters**: None.
**Content-Type**: `Text/CSV`.
Exports a projection of the database content.

# `catalog.php`
**Method**: `GET`.
**Parameters**: None.
**Content-Type**: `Application/JSON`.
Returns the list of available tickets.

# `info.php`
**Method**: `GET`.
**Parameters**: id.
**Content-Type**: `Application/JSON`.
Returns information on a specific registration.

# `initial-registration.php`
**Method**: `POST`.
**Parameters**: registration (request body).
**Content-Type**: `Application/JSON`.
Saves a registration and send a confirmation email to the registrant.
Returns the registration id.

Example of registration:
```json
{
  "personal-info": {
    "surname": "Jean",
    "name": "Valjean",
    "responsible": null
  },
  "contact": {
    "address": {
      "line1": "123 rue Plamondon",
      "line2": "",
      "postalCode": "G1J 5D2",
      "city": "Québec, QC",
      "country": "Canada"
    },
    "phone": "01 47 20 00 01",
    "email": "valjean@example.com"
  },
  "basket": [
    {
      "ticket": 6,
      "quantity": 2
    },
    {
      "ticket": 2,
      "quantity": 1
    }
  ],
  "workshops": [
    31,
    24,
    20,
    18,
    41,
    37
  ],
  "payment-mode": "transfer",
  "price-type": "full"
}
```

# `suggest.php`
**Method**: `GET`.
**Parameters**: term.
**Content-Type**: `Application/JSON`.
Suggests a list of registrations matching the *term*.

# `validate-payment.php`
**Method**: `POST`.
**Parameters**: id.
**Content-Type**: `Application/JSON`.
Marks a registration as `paid` and send a confirmation email to the registrant.

# `workshops.php`
**Method**: `GET`.
**Parameters**: None.
**Content-Type**: `Application/JSON`.
Returns the list of available workshops, associated with their remaining number of tickets.

