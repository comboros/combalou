<?php

require_once 'inc/config.php';

function err($msg, $code = 500) {
    http_response_code($code);
    echo $msg;
    exit();
}


function connect() {
    $mysqli = mysqli_connect(Config::db_url, Config::db_user, Config::db_passwd, Config::db_name);
    if (mysqli_connect_errno($mysqli)) {
        err('Échec lors de la connexion à MySQL : ' . mysqli_connect_error());
    }
    if (!$mysqli->set_charset(Config::db_charset)) {
        err("Échec lors de la définition de l’encodage de la connexion : $mysqli->error");
    }
    return $mysqli;
}
