<?php

require_once 'inc/config.php';
require_once 'inc/model.php';
require_once 'inc/repository.php';
require_once 'inc/utils.php';

$mysqli = connect();
$repository = new TicketRepository($mysqli);
$tickets = $repository->findAll();

header('Content-Type: application/json');
echo json_encode($tickets);

