<?php
/*
 * Delete a registration and all its relations.
 */
require_once 'inc/config.php';
require_once 'inc/model.php';
require_once 'inc/repository.php';
require_once 'inc/utils.php';

// receive

if (!isset($_GET['id']) || !($id = $_GET['id'])) {
    err('id query parameter is mandatory', 400);
}

// log

$mysqli = connect();
$repository = new RegistrationRepository($mysqli);
$synopsis = $repository->getSynopsis($id);
error_log("Deleting registration $id");
error_log(implode(' | ', $synopsis));

// persist

$repository->deleteOne($id);

http_response_code(200);
exit();
