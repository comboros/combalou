<?php
/*
 * Send the registration invoice by email.
 */
require_once 'inc/config.php';
require_once 'inc/model.php';
require_once 'inc/repository.php';
require_once 'inc/mailing-service.php';
require_once 'inc/utils.php';

if (!isset($_GET['id']) || !($id = $_GET['id'])) {
    err('id query parameter is mandatory', 400);
}

$mysqli = connect();

$repository = new RegistrationRepository($mysqli);
$registration = $repository->findOne($id);
if (!$registration) {
    error_log("Received unknown registration id $id");
    err("Registration $id not found", 404);
}

$mailingService = new MailingService(
    new TicketRepository($mysqli),
    new WorkshopRepository($mysqli)
);

try {
    $ret = $mailingService->sendConfirmation($registration, $id);
    if (!$ret) {
        error_log("Could not send an email to $registration->email ($id)");
    }
} catch (Exception $e) {
    error_log("Could not send an email to $registration->email ($id)");
    error_log($e->getMessage());
}

$repository->updateLastBilling($id);
