<?php
/*
 * Update the registration’s personal info (everything except basket and workshops).
 */
require_once 'inc/config.php';
require_once 'inc/model.php';
require_once 'inc/repository.php';
require_once 'inc/utils.php';

// receive

if (!isset($_GET['id']) || !($id = $_GET['id'])) {
    err('id query parameter is mandatory', 400);
}

$json = file_get_contents('php://input');
$obj = json_decode($json);

// validate

if (!$obj) {
    error_log('Received invalid JSON');
    err('Invalid JSON: ' . json_last_error_msg(), 400);
}

try {
    $dto = PersonalInfoDTO::fromJsonObject($obj);
} catch (Exception $e) {
    error_log($e->getMessage());
    err('Invalid data: ' . $e->getMessage(), 400);
}

// handle

$mysqli = connect();
$repository = new RegistrationRepository($mysqli);
$registration = $repository->findOne($id);
if (!$registration) {
    error_log("Received unknown registration id $id");
    err("Registration $id not found", 404);
}

// TODO: handle all specific cases

// persist

$repository->updatePersonalInfo($id, $dto);

http_response_code(204);
exit();

