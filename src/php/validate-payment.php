<?php
/**
 * POST
 *
 * Validate payment for a registration and send a confirmation email
 */

require_once 'inc/utils.php';
require_once 'inc/model.php';
require_once 'inc/repository.php';
require_once 'inc/payment-confirmation-service.php';

if (!isset($_GET['id']) || !($id = $_GET['id'])) {
    err('id query parameter is mandatory', 400);
}

$mysqli = connect();
$registrationRepository = new RegistrationRepository($mysqli);
$registrationRepository->changeState($id, 'paid');
$registration = $registrationRepository->findOne($id);
$service = new PaymentConfirmationService();
$service->sendConfirmation($registration->email);

http_response_code(204);
exit();

