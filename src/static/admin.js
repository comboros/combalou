'use strict';

require('./styles/main.scss');

const {Elm} = require('../Admin/Main');
Elm.Admin.Main.init({ node: document.getElementById('elm-container') });
