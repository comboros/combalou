// pull in desired CSS/SASS files
require('./styles/main.scss');

// inject bundled Elm app into div#main
var {Elm} = require('../Form/Main');
Elm.Form.Main.init({ node: document.getElementById('elm-container') });
