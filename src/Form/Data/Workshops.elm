module Form.Data.Workshops exposing
    ( Model
    , TrimmedWorkshop
    , decoder
    , empty
    , encode
    , fromList
    , getTotal
    , getWorkshops
    , map
    , selectWorkshop
    , selectedWorkshop
    , unselectWorkshop
    )

import AssocList as Dict exposing (Dict)
import Common.Category as Category exposing (Category(..))
import Common.Day as Day exposing (Day(..))
import Common.MaybeSelectList as MaybeSelectList exposing (MaybeSelectList)
import Common.Moment as Moment exposing (Moment(..))
import Common.Workshop as Workshop exposing (Workshop)
import Http
import Json.Decode as Decode exposing (Decoder, field, float, int, list, string, succeed)
import Json.Decode.Pipeline exposing (custom, optional, required)
import Json.Encode as Json



-- TYPES --


type alias TrimmedWorkshop =
    { id : Int
    , name : String
    , band : String
    , category : Category
    , remainder : Int
    }


type alias Model =
    Dict ( Day, Moment ) (MaybeSelectList TrimmedWorkshop)


empty : Model
empty =
    Dict.empty



-- DECODERS --


decoder : Decoder Model
decoder =
    list Workshop.decoder
        |> Decode.map fromList



-- ENCODERS --


encode : Model -> Json.Value
encode model =
    let
        workshopTitles =
            map (\( _, _, workshop ) -> workshop.id) model
    in
    Json.list Json.int workshopTitles



-- UTILS --


getWorkshops : Model -> Day -> Moment -> List TrimmedWorkshop
getWorkshops model day moment =
    Dict.get ( day, moment ) model
        |> Maybe.withDefault MaybeSelectList.empty
        |> MaybeSelectList.toList


selectWorkshop : Day -> Moment -> TrimmedWorkshop -> Model -> Model
selectWorkshop day moment workshop model =
    let
        newWorkshops =
            Dict.get ( day, moment ) model
                |> Maybe.withDefault MaybeSelectList.empty
                |> MaybeSelectList.select workshop
    in
    Dict.insert ( day, moment ) newWorkshops model


unselectWorkshop : Day -> Moment -> Model -> Model
unselectWorkshop day moment model =
    let
        newWorkshops =
            Dict.get ( day, moment ) model
                |> Maybe.withDefault MaybeSelectList.empty
                |> MaybeSelectList.unselect
    in
    Dict.insert ( day, moment ) newWorkshops model


selectedWorkshop : Day -> Moment -> Model -> Maybe TrimmedWorkshop
selectedWorkshop day moment model =
    Dict.get ( day, moment ) model
        |> Maybe.andThen MaybeSelectList.selected


fromList : List Workshop -> Model
fromList list =
    classifyWorkshops list
        |> Dict.map
            (\key values ->
                MaybeSelectList.fromList values
            )


map : (( Day, Moment, TrimmedWorkshop ) -> a) -> Model -> List a
map fn model =
    Dict.toList model
        |> List.filterMap
            (\( key, lst ) ->
                case key of
                    ( day, moment ) ->
                        case MaybeSelectList.selected lst of
                            Just workshop ->
                                Just ( day, moment, workshop )

                            Nothing ->
                                Nothing
            )
        |> List.map fn


trimWorkshop : Workshop -> TrimmedWorkshop
trimWorkshop w =
    { id = w.id
    , name = w.name
    , band = w.band
    , category = w.category
    , remainder = w.remainder
    }


classifyWorkshops : List Workshop -> Dict ( Day, Moment ) (List TrimmedWorkshop)
classifyWorkshops list =
    List.foldl
        (\raw dict ->
            let
                key =
                    ( raw.day, raw.moment )

                values =
                    Dict.get key dict
                        |> Maybe.withDefault []

                workshop =
                    trimWorkshop raw
            in
            Dict.insert key (workshop :: values) dict
        )
        Dict.empty
        list


getTotal : Model -> Int
getTotal model =
    map (\( _, _, _ ) -> 1) model
        |> List.sum
