module Form.Data.Catalog exposing (Catalog, Span(..), Ticket, decoder, emptyTicket, request)

import Common.Url as Url
import Http
import Json.Decode as Decode exposing (Decoder, field, float, int, list, maybe, string, succeed)
import Json.Decode.Pipeline exposing (custom, optional, required)



-- TYPES --


type alias Ticket =
    { id : Int
    , name : String
    , price : Float
    , reducedPrice : Float
    , description : Maybe String
    , span : Span
    , workshopsCount : Int
    }


type alias Catalog =
    List Ticket


emptyTicket : Ticket
emptyTicket =
    { id = 0
    , name = ""
    , price = 0
    , reducedPrice = 0
    , description = Nothing
    , span = Weekly
    , workshopsCount = 0
    }


type Span
    = Weekly
    | Daily
    | Unitary



-- DECODERS --


decoder : Decoder Catalog
decoder =
    list ticketDecoder


ticketDecoder : Decoder Ticket
ticketDecoder =
    succeed Ticket
        |> required "id" int
        |> required "name" string
        |> required "price" float
        |> required "reducedPrice" float
        |> optional "description" (maybe string) Nothing
        |> custom (field "span" spanDecoder)
        |> required "workshops" int


spanDecoder : Decoder Span
spanDecoder =
    Decode.string
        |> Decode.andThen
            (\str ->
                case str of
                    "weekly" ->
                        Decode.succeed Weekly

                    "daily" ->
                        Decode.succeed Daily

                    "unitary" ->
                        Decode.succeed Unitary

                    _ ->
                        Decode.fail <| "Unknown span: " ++ str
            )



-- HTTP --


request : Http.Request Catalog
request =
    Http.get Url.tickets decoder
