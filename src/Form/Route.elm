module Form.Route exposing (Route(..), fromUrl, href, newUrl)

import Browser.Navigation as Navigation
import Html exposing (Attribute)
import Html.Attributes as Attr
import Url exposing (Url)
import Url.Parser exposing ((</>), Parser, map, oneOf, s, string)



-- ROUTING --


type Route
    = Home
    | PersonalInfo
    | Contact
    | Tickets
    | Workshops
    | Summary
    | PaymentChoice
    | RegistrationResult



--    When needing parameters on the form base/item/id
--   | Item String


routeParser : Parser (Route -> a) a
routeParser =
    oneOf
        [ map Home (s "")
        , map PersonalInfo (s "personal-info")
        , map Contact (s "contact")
        , map Tickets (s "tickets")
        , map Workshops (s "workshops")
        , map Summary (s "summary")
        , map PaymentChoice (s "payment-choice")
        , map RegistrationResult (s "registration-result")

        --    When needing parameters on the form base/item/3
        --    , Url.map Item (s "item" </> string)
        ]



-- INTERNAL --


routeToString : Route -> String
routeToString page =
    let
        pagePath =
            case page of
                Home ->
                    [ "" ]

                PersonalInfo ->
                    [ "personal-info" ]

                Contact ->
                    [ "contact" ]

                Tickets ->
                    [ "tickets" ]

                Workshops ->
                    [ "workshops" ]

                Summary ->
                    [ "summary" ]

                PaymentChoice ->
                    [ "payment-choice" ]

                RegistrationResult ->
                    [ "registration-result" ]

        --    When needing parameters on the form base/item/3
        --                    Item id ->
        --                    [ "item",  id ]
    in
    "#/" ++ String.join "/" pagePath



-- PUBLIC HELPERS --


href : Route -> Attribute msg
href route =
    Attr.href (routeToString route)


fromUrl : Url -> Maybe Route
fromUrl url =
    -- Treat fragment as if it is the path
    { url
        | path = Maybe.withDefault "" url.fragment
        , fragment = Nothing
    }
        |> Url.Parser.parse routeParser


newUrl : Navigation.Key -> Route -> Cmd msg
newUrl key =
    routeToString >> Navigation.pushUrl key
