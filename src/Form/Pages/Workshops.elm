module Form.Pages.Workshops exposing (Model, Msg, Workshop, encode, update, view)

import Accessibility exposing (Html, a, article, button, div, fieldset, footer, h1, h2, h3, header, hr, label, legend, p, radio, section, span, strong, text)
import Accessibility.Landmark exposing (main_)
import Accessibility.Style exposing (invisible)
import Browser.Navigation as Navigation
import Common.Day as Day exposing (Day(..))
import Common.Moment as Moment exposing (Moment(..))
import Form.Data.Workshops exposing (..)
import Form.Route as Route
import Html.Attributes exposing (checked, class, classList, disabled, href, id, name, style, tabindex, target, type_, value)
import Html.Events exposing (onCheck, onClick)
import Json.Encode as Json



-- MODEL --


type alias Model =
    Form.Data.Workshops.Model


type alias Workshop =
    Form.Data.Workshops.TrimmedWorkshop


encode : Model -> Json.Value
encode =
    Form.Data.Workshops.encode



-- VIEW --


view : Int -> Model -> Html Msg
view workshopsCount model =
    let
        total =
            getTotal model
    in
    section [ class "section", main_, id "main-section", tabindex -1 ]
        [ div [ class "container" ] <|
            [ h1 [ class "title is-1" ] [ text "Ateliers" ] ]
                ++ [ p []
                        [ text "Pour plus de détails concernant un ateliers, consultez "
                        , a
                            [ href "https://www.comboros.com/2019/fr/stages"
                            , target "_blank"
                            ]
                            [ text "notre site internet" ]
                        , text "."
                        ]
                   ]
                ++ [ p [] [ text """Si une demi-journée est vide,
                c’est que tous les ateliers sur ce créneau sont pleins !
                Il n’y a pas d’atelier le jeudi matin.""" ] ]
                ++ [ hr [] [] ]
                ++ viewWorkshops model
                ++ [ div [ class "is-clearfix", style "margin-top" "2rem" ]
                        [ div [ class "buttons is-pulled-right" ]
                            [ button
                                [ type_ "button", class "button is-secondary", onClick Previous ]
                                [ text "Retour" ]
                            , button
                                [ type_ "submit"
                                , class "button is-primary"
                                , onClick Next
                                , disabled (total /= workshopsCount)
                                ]
                                [ text "Suite" ]
                            ]
                        ]
                   , div [ class "is-clearfix" ]
                        [ div [ class "is-pulled-right", style "margin-top" "0.5rem" ]
                            [ viewInvalidMessage workshopsCount total ]
                        ]
                   ]
        ]


viewWorkshops : Model -> List (Html Msg)
viewWorkshops workshops =
    List.map (dayToStringWorkshops workshops) Day.days


dayToStringWorkshops : Model -> Day -> Html Msg
dayToStringWorkshops workshops day =
    article []
        [ h2 [ class "title is-spaced" ]
            [ text <| Day.toString day ]
        , div [ class "columns" ]
            (List.map (dayToStringMomentWorkshops workshops day) Moment.moments)
        , hr [] []
        ]


dayToStringMomentWorkshops : Model -> Day -> Moment -> Html Msg
dayToStringMomentWorkshops workshops day moment =
    let
        slice =
            getWorkshops workshops day moment

        selection =
            selectedWorkshop day moment workshops

        hasSelection =
            Maybe.map (always True) selection
                |> Maybe.withDefault False
    in
    div [ class "column" ]
        [ h3 [ class "subtitle" ]
            [ text <| Moment.toString moment
            , button
                [ type_ "button"
                , class "button is-light is-small"
                , style "margin-left" "1rem"
                , onClick (Unselect day moment)
                , disabled (not hasSelection)
                ]
                [ text "Annuler" ]
            ]
        , case slice of
            [] ->
                p [ class "notification" ]
                    [ text "Il n’y a pas d’atelier disponible à ce moment là." ]

            _ ->
                fieldset [] <|
                    [ legend invisible
                        [ text <| "Choix de l’atelier du " ++ Day.toString day ++ " " ++ Moment.toString moment ]
                    ]
                        ++ (case selection of
                                Nothing ->
                                    List.map (\w -> viewWorkshop day moment w False) slice

                                Just x ->
                                    List.map (\w -> viewWorkshop day moment w (w == x)) slice
                           )
        ]


viewWorkshop : Day -> Moment -> Workshop -> Bool -> Html Msg
viewWorkshop day moment workshop isSelected =
    let
        inputGroup =
            workshop.name ++ Day.toString day ++ Moment.toString moment ++ "-radio-group"
    in
    div [ class "control", style "margin-bottom" "1rem" ]
        [ label
            -- We must use a class because “disabled” can’t be used on a label
            [ classList [ ( "radio", True ), ( "radio is-disabled", workshop.remainder <= 0 ) ] ]
            [ radio inputGroup
                ""
                isSelected
                [ onCheck (\_ -> Select day moment workshop)
                , disabled (workshop.remainder <= 0)
                ]
            , text " "
            , strong [] [ text workshop.name ]
            , span
                [ class "has-text-grey"
                , style "font-variant" "small-caps"
                ]
                [ text " animé par " ]
            , text workshop.band
            ]
        ]


viewInvalidMessage : Int -> Int -> Html msg
viewInvalidMessage workshopsCount total =
    if total < workshopsCount then
        p [ class "has-text-info has-text-right" ]
            [ text <|
                "Vous n’avez pas choisi suffisament d’ateliers !"
                    ++ " Il vous en reste "
                    ++ (String.fromInt <| workshopsCount - total)
                    ++ " à sélectionner 😉"
            ]

    else if total > workshopsCount then
        p [ class "has-text-info has-text-right" ]
            [ text <|
                "Vous avez choisi "
                    ++ (String.fromInt <| total - workshopsCount)
                    ++ " ateliers de trop !"
            ]

    else
        p [] []



-- UPDATE --


type Msg
    = Next
    | Previous
    | Select Day Moment Workshop
    | Unselect Day Moment


update : Navigation.Key -> Msg -> Model -> ( Model, Cmd Msg )
update key msg model =
    case msg of
        Next ->
            ( model, Route.newUrl key Route.Summary )

        Previous ->
            ( model, Route.newUrl key Route.Tickets )

        Select day moment workshop ->
            ( selectWorkshop day moment workshop model
            , Cmd.none
            )

        Unselect day moment ->
            ( unselectWorkshop day moment model
            , Cmd.none
            )
