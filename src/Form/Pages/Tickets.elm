module Form.Pages.Tickets exposing (Model, Msg, TicketSelection, compareTickets, encode, init, selection, total, update, validate, view, workshopsCount)

import Accessibility exposing (Html, br, button, div, footer, h3, header, li, p, section, span, strong, text, ul)
import Accessibility.Key exposing (tabbable)
import Accessibility.Landmark exposing (main_)
import Accessibility.Style exposing (invisible)
import Accessibility.Widget exposing (hasDialogPopUp, hidden, pressed)
import AssocList as Dict exposing (Dict)
import Browser.Dom as Dom
import Browser.Navigation as Navigation
import Common.Basket exposing (maxWorkshops)
import Form.Data.Catalog as Catalog exposing (..)
import Form.Pages.TicketSelectionModal as Modal
import Form.Route as Route
import Html
import Html.Attributes exposing (class, classList, disabled, id, style, tabindex, type_, value)
import Html.Events exposing (onClick)
import Json.Encode as Json
import Task



-- MODEL --


type alias TicketSelection =
    { bundle : Maybe Ticket
    , unitaries : Dict Ticket Int
    }


type alias Model =
    { selection : TicketSelection
    , modal : Modal.Model
    , isReduced : Maybe Bool
    , catalog : Catalog
    }


init : Catalog -> Model
init catalog =
    { selection =
        { bundle = Nothing
        , unitaries = Dict.empty
        }
    , modal = Modal.init
    , isReduced = Nothing
    , catalog = catalog
    }


selection : Model -> List ( Ticket, Int )
selection model =
    Maybe.map (\t -> [ ( t, 1 ) ]) model.selection.bundle
        |> Maybe.withDefault []
        |> List.append (Dict.toList model.selection.unitaries)
        |> List.filter (\( _, quantity ) -> quantity > 0)


total : Model -> Float
total model =
    let
        priceGetter =
            if Maybe.withDefault False model.isReduced then
                .reducedPrice

            else
                .price
    in
    selection model
        |> List.map (\( ticket, quantity ) -> priceGetter ticket * toFloat quantity)
        |> List.foldl (+) 0


workshopsCount : Model -> Int
workshopsCount model =
    selection model
        |> List.foldl (\( t, q ) sum -> sum + t.workshopsCount * q) 0


encode : Model -> Json.Value
encode model =
    selection model
        |> Json.list encodeEntry


encodeEntry : ( Ticket, Int ) -> Json.Value
encodeEntry ( t, q ) =
    Json.object
        [ ( "ticket", Json.int t.id )
        , ( "quantity", Json.int q )
        ]


compareTickets : Ticket -> Ticket -> Order
compareTickets a b =
    case compareSpans a.span b.span of
        EQ ->
            compare a.price b.price

        GT ->
            GT

        LT ->
            LT


compareSpans : Span -> Span -> Order
compareSpans a b =
    let
        order =
            \x ->
                case x of
                    Unitary ->
                        1

                    Daily ->
                        2

                    Weekly ->
                        3
    in
    compare (order a) (order b)



-- VIEW --


viewReducedFeeForm : Model -> Html Msg
viewReducedFeeForm model =
    let
        isReduced =
            Maybe.withDefault False model.isReduced
    in
    div []
        [ div [ class "field is-grouped is-grouped-centered" ]
            [ div [ class "control" ]
                [ button
                    [ class "button is-info"
                    , classList [ ( "is-focused", not isReduced ) ]
                    , onClick (SetIsReduced False)
                    , tabbable <| not (Modal.isActive model.modal)
                    , pressed <| Maybe.map not model.isReduced
                    ]
                    [ text "Je ne suis pas concerné" ]
                ]
            , div [ class "control" ]
                [ button
                    [ class "button"
                    , classList [ ( "is-focused", isReduced ) ]
                    , onClick (SetIsReduced True)
                    , tabbable <| not (Modal.isActive model.modal)
                    , pressed <| model.isReduced
                    ]
                    [ text "J’en bénéficie" ]
                ]
            ]
        , div [ class "has-text-centered" ]
            [ p [ class "help" ]
                [ text "Si vous bénéficiez d’un tarif réduit, n’oubliez pas d’amener un justificatif !" ]
            ]
        ]


viewTicketPrice : Int -> Model -> Ticket -> List (Html Msg)
viewTicketPrice count model ticket =
    let
        isReduced =
            Maybe.withDefault False model.isReduced

        price =
            if isReduced then
                ticket.reducedPrice

            else
                ticket.price

        priceAddon =
            if isReduced then
                [ text "Tarif plein"
                , br []
                , text <| String.fromFloat ticket.price ++ " €"
                ]

            else
                []
    in
    [ p []
        [ span [ class "is-size-3" ]
            [ text <| String.fromFloat price ++ " €" ]
        , span
            [ style "position" "absolute"
            , classList
                [ ( "tag is-info", True )
                , ( "is-invisible", count <= 0 )
                ]
            ]
            [ text (String.fromInt count) ]
        ]
    , p [ class "has-text-grey-dark", hidden True ] priceAddon
    ]


viewBundleTicket : Model -> Ticket -> Html Msg
viewBundleTicket model ticket =
    let
        isSelected =
            Maybe.map (\t -> t == ticket) model.selection.bundle
                |> Maybe.withDefault False
    in
    div
        [ classList
            [ ( "card bundle", True )
            , ( "selected", isSelected )
            ]
        ]
        [ header [ class "card-header" ]
            [ p [ class "card-header-title is-centered" ]
                [ text ticket.name ]
            ]
        , div [ class "card-content" ]
            [ div [ class "content has-text-centered" ]
                ([ p [ class "is-size-7", style "min-height" "5.7rem" ]
                    (Maybe.map text ticket.description
                        |> Maybe.map List.singleton
                        |> Maybe.withDefault []
                    )
                 ]
                    ++ viewTicketPrice 0 model ticket
                )
            ]
        , footer [ class "card-footer" ]
            [ button
                [ class "card-footer-item button is-white has-text-link"
                , onClick
                    (if isSelected then
                        DropBundle

                     else
                        PickBundle ticket
                    )
                , tabbable <| not (Modal.isActive model.modal)
                , pressed <| Just isSelected
                , id <| ticketId ticket
                ]
                [ text <|
                    if isSelected then
                        "Déselectionner"

                    else
                        "Sélectionner"
                ]
            ]
        ]


viewUnitaryTicket : Model -> Ticket -> Html Msg
viewUnitaryTicket model ticket =
    let
        count =
            Dict.get ticket model.selection.unitaries
                |> Maybe.withDefault 0

        cardClasses =
            if count > 0 then
                "card ticket selected"

            else
                "card ticket"

        descriptionHeight =
            Maybe.map (always (style "min-height" "5.7rem")) ticket.description
                |> Maybe.withDefault (style "margin" "0")
    in
    div [ class cardClasses ]
        [ header [ class "card-header" ]
            [ p [ class "card-header-title is-centered" ]
                [ text ticket.name ]
            ]
        , div [ class "card-content" ]
            [ div [ class "content has-text-centered" ]
                ([ p [ class "is-size-7", descriptionHeight ]
                    (Maybe.map text ticket.description
                        |> Maybe.map List.singleton
                        |> Maybe.withDefault []
                    )
                 ]
                    ++ viewTicketPrice count model ticket
                )
            ]
        , footer [ class "card-footer" ]
            [ button
                [ class "card-footer-item button is-white has-text-link"
                , onClick (ShowModal ticket)
                , hasDialogPopUp
                , tabbable <| not (Modal.isActive model.modal)
                , id <| ticketId ticket
                ]
                [ text "Sélectionner" ]
            ]
        ]


viewTickets : (Model -> Ticket -> Html Msg) -> Model -> List Ticket -> Html Msg
viewTickets renderer model tickets =
    div [ class "columns is-centered" ]
        (List.map
            (\x ->
                div [ class "column is-one-quarter" ]
                    [ renderer model x ]
            )
            tickets
        )


view : Model -> Html Msg
view model =
    let
        visibility =
            if model.isReduced == Nothing then
                "none"

            else
                "block"

        errorMessage =
            validate model

        isValid =
            maybeIsEmpty errorMessage

        weeklyTickets =
            List.filter (\x -> x.span == Weekly) model.catalog

        dailyTickets =
            List.filter (\x -> x.span == Daily) model.catalog

        unitaryTickets =
            List.filter (\x -> x.span == Unitary) model.catalog
    in
    section [ class "section", main_, id "main-section", tabindex -1 ]
        [ div [ class "container" ]
            [ presentationText
            , h3 [ class "title" ]
                [ text "Bénéficiez-vous d’un tarif réduit ?" ]
            , reducedFeeText
            , viewReducedFeeForm model
            , div [ style "display" visibility ]
                [ h3 [ class "title" ]
                    [ text "Formules" ]
                , ticketsPresentationText
                , viewTickets viewBundleTicket model weeklyTickets
                , viewTickets viewBundleTicket model dailyTickets
                , h3 [ class "title" ]
                    [ text "Stages supplémentaires" ]
                , viewTickets viewUnitaryTicket model unitaryTickets
                ]
            , div [ class "is-clearfix", style "margin-top" "2rem" ]
                [ div [ class "buttons is-pulled-right" ]
                    [ button
                        [ type_ "button"
                        , class "button is-secondary"
                        , onClick Previous
                        , tabbable <| not (Modal.isActive model.modal)
                        ]
                        [ text "Retour" ]
                    , button
                        [ type_ "submit"
                        , class "button is-primary"
                        , onClick Next
                        , disabled (not isValid)
                        , tabbable <| not (Modal.isActive model.modal)
                        ]
                        [ text "Suite" ]
                    ]
                ]
            , viewInvalidMessage errorMessage
            ]
        , Modal.view model.modal
            |> Html.map ModalMsg
        ]


presentationText : Html Msg
presentationText =
    div [ class "content" ]
        [ p []
            [ text "Il vous faut à présent choisir vos billets ! "
            , text "Pour cela, vous avez le choix entre différents "
            , strong []
                [ text "forfaits festival "
                ]
            , text "qui vous offrent la possibiliter d’assister à "
            , strong []
                [ text "tout Comboros "
                ]
            , text "! "
            , text "Vous pourrez ensuite compléter votre forfait par des "
            , strong []
                [ text "stages vendus à l’unité"
                ]
            , text " (certains forfaits en incluent déjà)."
            ]
        ]


reducedFeeText : Html Msg
reducedFeeText =
    div [ class "content" ]
        [ p []
            [ text "Pour bénéficier d’un tarif réduit, "
            , text "il vous suffit de remplir l’une des conditions suivantes\u{00A0}:"
            , ul []
                [ li [] [ text "Adhérent-e Brayauds–CDMDT 63" ]
                , li [] [ text "Habitant-e de la commune de Saint-Gervais-d’Auvergne" ]
                , li [] [ text "Mineur-e" ]
                , li [] [ text "Étudiant-e de moins de 26 ans" ]
                , li [] [ text "Chomeur-eus-e" ]
                , li [] [ text "Âgé-e de plus de 70 ans" ]
                , li [] [ text "Détenteur-ice d’une carte d’invalidité" ]
                ]
            ]
        ]


ticketsPresentationText : Html Msg
ticketsPresentationText =
    div [ class "message is-warning" ]
        [ div [ class "message-body" ]
            [ text <|
                "L’entrée aux soirées du jeudi soir à Gouttières et Vergheas, "
                    ++ "organisées par nos partenaires, n’est pas comprise dans les forfaits."
            ]
        ]


viewInvalidMessage : Maybe String -> Html msg
viewInvalidMessage errorMessage =
    case errorMessage of
        Nothing ->
            p [] []

        Just msg ->
            p [ class "has-text-info has-text-right" ]
                [ text msg ]


ticketId : Ticket -> String
ticketId t =
    "ticket-" ++ String.fromInt t.id



-- UPDATE --


type Msg
    = SetIsReduced Bool
    | PickBundle Ticket
    | DropBundle
    | ShowModal Ticket
    | ModalMsg Modal.Msg
    | Previous
    | Next
    | NoOp (Result Dom.Error ())


update : Navigation.Key -> Msg -> Model -> ( Model, Cmd Msg )
update key msg model =
    case msg of
        SetIsReduced isReduced ->
            ( { model | isReduced = Just isReduced }, Cmd.none )

        PickBundle ticket ->
            let
                oldSelection =
                    model.selection

                newSelection =
                    { oldSelection | bundle = Just ticket }
            in
            ( { model | selection = newSelection }, Cmd.none )

        DropBundle ->
            let
                oldSelection =
                    model.selection

                newSelection =
                    { oldSelection | bundle = Nothing }
            in
            ( { model | selection = newSelection }, Cmd.none )

        ShowModal ticket ->
            let
                dict =
                    model.selection.unitaries

                modal =
                    { ticket = ticket
                    , count =
                        Dict.get ticket dict
                            |> Maybe.withDefault 0
                    , isReduced = Maybe.withDefault False model.isReduced
                    }
            in
            ( { model | modal = Just modal }, Cmd.none )

        ModalMsg modalMsg ->
            let
                ( newModal, action ) =
                    Modal.update modalMsg model.modal
            in
            case action of
                Modal.Validate ticket count ->
                    let
                        newDict =
                            Dict.insert ticket count model.selection.unitaries

                        oldSelection =
                            model.selection

                        newSelection =
                            { oldSelection | unitaries = newDict }
                    in
                    ( { model
                        | selection = newSelection
                        , modal = newModal
                      }
                    , restoreFocus ticket
                    )

                Modal.Cancel ticket ->
                    ( { model | modal = newModal }, restoreFocus ticket )

                Modal.NoOp ->
                    ( { model | modal = newModal }, Cmd.none )

        Previous ->
            ( model, Route.newUrl key Route.Contact )

        Next ->
            let
                nextRoute =
                    if workshopsCount model > 0 then
                        Route.Workshops

                    else
                        Route.Summary
            in
            ( model, Route.newUrl key nextRoute )

        NoOp _ ->
            ( model, Cmd.none )


restoreFocus : Ticket -> Cmd Msg
restoreFocus ticket =
    Task.attempt NoOp (Dom.focus <| ticketId ticket)



-- VALIDATE --


maybeIsEmpty : Maybe a -> Bool
maybeIsEmpty maybe =
    case maybe of
        Nothing ->
            True

        _ ->
            False


ε : Float
ε =
    10 ^ -3


validate : Model -> Maybe String
validate model =
    let
        count =
            workshopsCount model
    in
    if maybeIsEmpty model.isReduced then
        Just "Vous n’avez pas précisé si vous bénéficiez d’un tarif réduit"

    else if maybeIsEmpty model.selection.bundle then
        Just "Vous n’avez pas sélectionné de forfait"

    else if count > maxWorkshops then
        Just <| "Vous avez choisi " ++ String.fromInt (count - maxWorkshops) ++ " ateliers de trop"

    else if total model < ε then
        Just "Votre panier est vide ! Choisissez au moins un forfait payant ou des ateliers"

    else
        Nothing
