module Form.Pages.Common exposing (viewFetchError, viewSpinner)

import Accessibility as Html exposing (Html, a, article, blockquote, button, div, p, span, text)
import Accessibility.Live exposing (busy)
import Accessibility.Style exposing (invisible)
import Common.View exposing (viewErrorExplanation, viewRetryLink)
import Html.Attributes exposing (class, href)
import Html.Events exposing (onClick)
import Http exposing (Error(..))


viewSpinner : Html msg
viewSpinner =
    div [ class "container", busy True ]
        [ p invisible [ text "Chargement" ]
        , div [ class "spinner" ]
            [ div [ class "bouncer is-primary" ] []
            , div [ class "bouncer is-primary is-delayed" ] []
            ]
        ]


viewFetchError : Error -> msg -> Html msg
viewFetchError err retry =
    article [ class "section message is-danger" ]
        [ div [ class "message-header" ]
            [ p []
                [ text "Je n’ai pas pu accéder au serveur !" ]
            ]
        , div [ class "message-body" ] <|
            [ p []
                [ text "Vous n’allez pas pouvoir continuer votre inscription :(" ]
            ]
                ++ viewErrorExplanation err
                ++ [ viewRetryLink err retry
                   , viewSupportEmail
                   ]
        ]


viewSupportEmail : Html msg
viewSupportEmail =
    p [ class "is-size-7" ]
        [ text "En cas de problème, vous pouvez nous contacter à l’adresse suivante\u{00A0}: "
        , a [ href "mailto:support@comboros.com" ] [ text "support@comboros.com" ]
        ]
