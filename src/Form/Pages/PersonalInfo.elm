module Form.Pages.PersonalInfo exposing (Model, Msg, encode, init, update, validate, view)

import Accessibility as Html exposing (Html, button, checkbox, div, h3, inputText, label, legend, p, section, text)
import Accessibility.Aria exposing (describedBy, errorMessage)
import Accessibility.Landmark exposing (main_)
import Accessibility.Widget exposing (invalid)
import AssocList as Dict exposing (Dict)
import Browser.Navigation as Navigation
import Form.Route as Route
import Html exposing (form)
import Html.Attributes exposing (action, checked, class, classList, disabled, for, id, name, placeholder, tabindex, type_, value)
import Html.Events exposing (onCheck, onInput, onSubmit)
import Json.Encode as Json
import Validate



-- MODEL --


type Field
    = Surname
    | Name
    | Responsible


type alias Model =
    { surname : String
    , name : String
    , major : Bool
    , responsible : String
    , errors : Dict Field String
    }


init : Model
init =
    { surname = ""
    , name = ""
    , major = False
    , responsible = ""
    , errors = Dict.empty
    }


encode : Model -> Json.Value
encode model =
    Json.object
        [ ( "surname", Json.string model.surname )
        , ( "name", Json.string model.name )
        , ( "responsible"
          , if model.major then
                Json.null

            else
                Json.string model.responsible
          )
        ]



-- VIEW --


view : Model -> Html Msg
view model =
    section [ class "section", main_, id "main-section", tabindex -1 ]
        [ div [ class "container" ]
            [ viewForm model ]
        ]


viewForm : Model -> Html Msg
viewForm model =
    form [ name "personal-info", onSubmit Next ]
        [ h3 [ class "title" ]
            [ text "État civil" ]
        , viewSurnameInput model
        , viewNameInput model
        , viewMajorityCheckbox model
        , viewResponsibleInput model
        , div [ class "field is-grouped is-grouped-right" ]
            [ button
                [ type_ "submit", class "button is-primary" ]
                [ text "Suite" ]
            ]
        ]


inputClass : Maybe String -> String
inputClass =
    Maybe.map (always "input is-danger") >> Maybe.withDefault "input"


viewSurnameInput : Model -> Html Msg
viewSurnameInput model =
    let
        error =
            Dict.get Surname model.errors
    in
    div [ class "field" ]
        [ label [ class "label mandatory", for "input-text-surname" ]
            [ text "Prénom" ]
        , div [ class "control" ]
            [ inputText
                model.surname
                [ class (inputClass error)
                , placeholder "Camille"
                , onInput SetSurname
                , id "input-text-surname"
                , invalid (error /= Nothing)
                , errorMessage "input-text-surname-error"
                ]
            ]
        , p [ class "help is-danger", id "input-text-surname-error" ]
            (Maybe.map (List.singleton << text) error
                |> Maybe.withDefault []
            )
        ]


viewNameInput : Model -> Html Msg
viewNameInput model =
    let
        error =
            Dict.get Name model.errors
    in
    div [ class "field" ]
        [ label [ class "label mandatory", for "input-name-text" ]
            [ text "Name" ]
        , div [ class "control" ]
            [ inputText
                model.name
                [ class (inputClass error)
                , placeholder "Wang"
                , onInput SetName
                , id "input-name-text"
                , invalid (error /= Nothing)
                , errorMessage "input-text-name-error"
                ]
            ]
        , p [ class "help is-danger", id "input-text-name-error" ]
            (Maybe.map (List.singleton << text) error
                |> Maybe.withDefault []
            )
        ]


viewMajorityCheckbox : Model -> Html Msg
viewMajorityCheckbox model =
    div [ class "field" ]
        [ legend [ class "label mandatory", for "input-checkbox-majority" ]
            [ text "Êtes-vous majeur-e ?" ]
        , label [ class "checkbox" ]
            [ checkbox ""
                (Just model.major)
                [ type_ "checkbox"
                , onCheck SetMajor
                , id "input-checkbox-majority"
                ]
            , text " J’ai plus de 18 ans"
            ]
        ]


responsibleAdultWarning : String
responsibleAdultWarning =
    "Pour les mineur-e-s de moins de 12 ans, "
        ++ "l'accompagnement par un-e adulte est obligatoire pour tous les ateliers. "
        ++ "Son nom sera demandé et inscrit à la billetterie. "
        ++ "Les mineur-e-s de 12 à 18 ans demeurent sous l'entière responsabilité de l'adulte responsable. "


viewResponsibleInput : Model -> Html Msg
viewResponsibleInput model =
    let
        error =
            Dict.get Responsible model.errors
    in
    div [ class "field" ]
        [ label
            [ class "label"
            , classList [ ( "mandatory", not model.major ) ]
            , for "input-text-responsible"
            ]
            [ text "Nom de l’adulte responsable" ]
        , div
            [ class "control" ]
            [ inputText
                model.responsible
                [ class (inputClass error)
                , onInput SetResponsible
                , disabled model.major
                , id "input-text-responsible"
                , describedBy [ "input-text-responsible-help" ]
                , invalid (error /= Nothing)
                , errorMessage "input-text-responsible-error"
                ]
            ]
        , p [ class "help is-danger", id "input-text-responsible-error" ]
            (Maybe.map (List.singleton << text) error
                |> Maybe.withDefault []
            )
        , p [ class "help", id "input-text-responsible-help" ]
            [ text responsibleAdultWarning
            ]
        ]



-- UPDATE --


type Msg
    = Next
    | SetSurname String
    | SetName String
    | SetMajor Bool
    | SetResponsible String


update : Navigation.Key -> Msg -> Model -> ( Model, Cmd a )
update key msg model =
    case msg of
        SetSurname surname ->
            ( { model | surname = surname }
            , Cmd.none
            )

        SetName name ->
            ( { model | name = name }
            , Cmd.none
            )

        SetMajor major ->
            ( { model | major = major }
            , Cmd.none
            )

        SetResponsible name ->
            ( { model | responsible = name }
            , Cmd.none
            )

        Next ->
            case Validate.validate formValidator model of
                Ok _ ->
                    ( model, Route.newUrl key Route.Contact )

                Err errorList ->
                    ( { model | errors = Dict.fromList errorList }
                    , Cmd.none
                    )



-- VALIDATE --


validate : Model -> Dict Field String
validate model =
    case Validate.validate formValidator model of
        Ok _ ->
            Dict.empty

        Err errorList ->
            Dict.fromList errorList


formValidator : Validate.Validator ( Field, String ) Model
formValidator =
    Validate.all
        [ Validate.ifBlank .surname ( Surname, requiredMsg )
        , Validate.ifBlank .name ( Name, requiredMsg )
        , Validate.fromErrors
            (\model ->
                if not model.major && String.isEmpty model.responsible then
                    [ ( Responsible, requiredMsg ) ]

                else
                    []
            )
        ]


requiredMsg : String
requiredMsg =
    "Ce champ est obligatoire"
