module Form.Pages.RegistrationResult exposing (Model, view)

import Accessibility exposing (Html, a, blockquote, br, caption, code, div, h3, p, pre, section, span, strong, table, tbody, td, text, th, tr)
import Accessibility.Landmark exposing (main_)
import Form.Pages.PaymentChoice exposing (PaymentType(..))
import Html.Attributes exposing (class, colspan, href, id, tabindex)


type alias Model =
    { choice : PaymentType
    , amount : Float
    , registrationLabel : String
    }


view : Model -> Html msg
view model =
    section [ class "section", main_, id "main-section", tabindex -1 ]
        [ div [ class "container" ]
            [ h3 [ class "title" ]
                [ text "Félicitations !" ]
            , p [ class "subtitle" ]
                [ text "Votre inscription a bien été prise en compte." ]
            , p [ class "notification is-success is-size-5 has-text-weight-bold has-text-centered" ]
                [ text "Les informations qui suivent vous ont été envoyées par email." ]
            , viewChoiceMessage model
            ]
        ]


viewChoiceMessage : Model -> Html msg
viewChoiceMessage model =
    case model.choice of
        Check ->
            div [ class "content" ]
                [ p []
                    [ text "Il vous reste à présent à envoyer votre chèque d’un montant de "
                    , strong [] [ text <| String.fromFloat model.amount ++ " €" ]
                    , text " à l’"
                    , strong [] [ text "ordre" ]
                    , text " de "
                    ]
                , blockquote [] [ text "Les Brayauds — CDMDT–63" ]
                , p []
                    [ strong [ class "has-text-danger" ]
                        [ text " en notant au dos l’intitulé suivant : " ]
                    ]
                , blockquote [] [ text model.registrationLabel ]
                , p []
                    [ text " à l’"
                    , strong [] [ text "adresse" ]
                    , text " suivante\u{00A0}:"
                    ]
                , blockquote []
                    [ text "Les Brayauds — CDMDT–63"
                    , br []
                    , text "40, rue de la République"
                    , br []
                    , text "63200 Saint-Bonnet-Près-Riom"
                    ]
                , viewFooter
                ]

        Transfer ->
            div [ class "content" ]
                [ p []
                    [ text "Il vous reste à présent à effectuer un virement d’un montant de "
                    , strong [] [ text <| String.fromFloat model.amount ++ " €" ]
                    , strong [ class "has-text-danger" ]
                        [ text " avec l’intitulé suivant : " ]
                    ]
                , pre []
                    [ text model.registrationLabel ]
                , p []
                    [ text " à destination de :" ]
                , table [ class "table" ]
                    [ caption [] [ text "Coordonnées bancaires" ]
                    , tbody []
                        [ tr []
                            [ td [] []
                            , th [] [ text "Banque" ]
                            , th [] [ text "Guichet" ]
                            , th [] [ text "Numéro de compte" ]
                            , th [] [ text "Clé" ]
                            ]
                        , tr []
                            [ th [] [ text "RIB France" ]
                            , td [] [ code [] [ text "16806" ] ]
                            , td [] [ code [] [ text "02200" ] ]
                            , td [] [ code [] [ text "66080092190" ] ]
                            , td [] [ code [] [ text "40" ] ]
                            ]
                        , tr []
                            [ th [] [ text "IBAN Étranger" ]
                            , td [ colspan 4 ]
                                [ code [] [ text "FR76 1680 6022 0066 0800 9219 040" ] ]
                            ]
                        , tr []
                            [ th [] [ text "BIC" ]
                            , td [ colspan 4 ]
                                [ code [] [ text "AGRIFRPP868" ] ]
                            ]
                        ]
                    ]
                , viewFooter
                ]


viewFooter : Html msg
viewFooter =
    p []
        [ text "N’hésitez pas à nous suivre "
        , a [ href "https://www.facebook.com/groups/comboros/" ]
            [ text "sur facebook" ]
        , text " et à vous promener sur "
        , a [ href "http://www.comboros.com" ]
            [ text "notre site" ]
        , text "\u{00A0}!"
        ]
