module Form.Pages.PaymentChoice exposing (Model, Msg(..), PaymentType(..), encode, init, update, view)

import Accessibility exposing (Html, button, div, h2, p, section, span, strong, text)
import Accessibility.Landmark exposing (main_)
import Accessibility.Widget exposing (pressed)
import Browser.Navigation as Navigation
import Form.Route as Route
import Html.Attributes exposing (attribute, class, classList, id, style, tabindex, type_, value)
import Html.Events exposing (onClick)
import Json.Encode as Json



-- MODEL --


type alias Model =
    PaymentType


type PaymentType
    = Check
    | Transfer


init : Model
init =
    Check


encode : Model -> Json.Value
encode model =
    Json.string <|
        case model of
            Check ->
                "check"

            Transfer ->
                "transfer"



-- VIEW --


view : Model -> Float -> Html Msg
view model total =
    let
        price =
            String.fromFloat total ++ " €"
    in
    section [ class "section", main_, id "main-section", tabindex -1 ]
        [ div [ class "container" ]
            [ h2 [ class "title is-2" ]
                [ text "Mode de paiement" ]
            , paymentInstructions
            , viewSpecificInstructions model
            , div
                [ class "buttons has-addons is-centered"
                , style "padding-top" "1.5rem"
                ]
                [ button
                    [ type_ "button"
                    , class "button is-large"
                    , classList [ ( "is-selected is-focused", model == Transfer ) ]
                    , onClick (Select Transfer)
                    , pressed <| Just (model == Transfer)
                    ]
                    [ text "Virement" ]
                , button
                    [ type_ "button"
                    , class "button is-large"
                    , classList [ ( "is-selected is-focused", model == Check ) ]
                    , onClick (Select Check)
                    , pressed <| Just (model == Check)
                    ]
                    [ text "Chèque postal" ]
                ]
            , p
                [ class "buttons is-centered"
                , style "padding-top" "1.5rem"
                ]
                [ button
                    [ type_ "submit"
                    , class "button is-large is-primary"
                    , onClick Next
                    ]
                    [ text "Continuer" ]
                ]
            , div
                [ class "field is-grouped is-grouped-right"
                , style "padding-top" "1.5rem"
                ]
                [ button
                    [ type_ "button", class "button is-secondary", onClick Previous ]
                    [ text "Retour" ]
                ]
            ]
        ]


paymentInstructions : Html msg
paymentInstructions =
    div [ class "content" ]
        [ p []
            [ text """Votre inscription ne sera prise en compte qu’une fois le versement reçu.
Vous disposez pour deux modes de paiement\u{00A0}:
par virement bancaire ou par chèque postal."""
            ]
        ]


viewSpecificInstructions : Model -> Html msg
viewSpecificInstructions choice =
    div [ class "content" ] <|
        case choice of
            Check ->
                [ p []
                    [ text """Vous nous ferez parvenir votre règlement par courrier
à l’adresse des Brayauds """
                    , strong []
                        [ text "en notant au dos un intitulé spécifique" ]
                    , text "."
                    ]
                ]

            Transfer ->
                [ p []
                    [ text "Vous nous ferez parvenir votre règlement par un virement "
                    , strong []
                        [ text "doté d’un intitulé spécifique" ]
                    , text "."
                    ]
                ]



-- UPDATE --


type Msg
    = Previous
    | Next
    | Select PaymentType


update : Navigation.Key -> Msg -> Model -> ( Model, Cmd Msg )
update key msg model =
    case msg of
        Previous ->
            ( model, Route.newUrl key Route.Summary )

        Next ->
            ( model, Route.newUrl key Route.RegistrationResult )

        Select choice ->
            ( choice, Cmd.none )
