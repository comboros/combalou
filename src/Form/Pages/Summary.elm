module Form.Pages.Summary exposing (Model, Msg, update, view)

import Accessibility exposing (Html, button, div, h1, h2, p, section, span, table, tbody, td, text, tfoot, th, thead, tr)
import Accessibility.Landmark exposing (main_)
import AssocList as Dict
import Browser.Navigation as Navigation
import Common.Day as Day exposing (Day(..))
import Common.Moment as Moment exposing (Moment(..))
import Form.Data.Catalog exposing (Ticket)
import Form.Data.Workshops as Workshops exposing (TrimmedWorkshop)
import Form.Pages.Contact as Contact
import Form.Pages.PersonalInfo as PersonalInfo
import Form.Pages.Tickets as Tickets exposing (workshopsCount)
import Form.Pages.Workshops as WorkshopsPage
import Form.Route as Route
import Html.Attributes exposing (class, classList, disabled, id, style, tabindex, type_, value)
import Html.Events exposing (onClick)



-- MODEL --


type alias Model =
    { info : PersonalInfo.Model
    , contact : Contact.Model
    , basket : Tickets.Model
    , workshops : Workshops.Model
    }


type Section
    = PersonalInfo
    | Contact
    | Basket
    | Workshops


type alias Workshop =
    TrimmedWorkshop



-- VIEW --


view : Model -> Html Msg
view model =
    let
        invalidSections =
            validate model

        isInvalid =
            not (List.isEmpty invalidSections)
    in
    section [ class "section", main_, id "main-section", tabindex -1 ]
        [ h1 [ class "title is-2" ] [ text "Récapitulatif" ]
        , viewIntroduction
        , viewInfoSection model.info (not (List.member PersonalInfo invalidSections))
        , viewContactSection model.contact (not (List.member Contact invalidSections))
        , viewBasketSection model.basket (not (List.member Basket invalidSections))
        , viewWorkshopsSection model.workshops
        , div [ class "container is-clearfix", style "margin-top" "2rem" ]
            [ div [ class "buttons is-pulled-right" ]
                [ button
                    [ type_ "button", class "button is-secondary", onClick Previous ]
                    [ text "Retour" ]
                , button
                    [ type_ "submit"
                    , class "button is-primary"
                    , onClick Next
                    , disabled isInvalid
                    ]
                    [ text "Suite" ]
                ]
            ]
        ]


viewIntroduction : Html msg
viewIntroduction =
    div [ class "container" ]
        [ p [ style "padding-bottom" "1.5rem" ]
            [ text "Merci de bien vérifier que les informations suivantes sont valides" ]
        ]


viewTitle : String -> Route.Route -> Bool -> Html Msg
viewTitle title route isValid =
    h2 [ class "title is-3" ]
        [ text title
        , span
            [ class "tag is-danger"
            , classList [ ( "is-hidden", isValid ) ]
            , style "margin-left" "1rem"
            ]
            [ text "Invalide" ]
        , button
            [ class "button is-light is-small"
            , style "margin-left" "1rem"
            , style "margin-top" "0.3rem"
            , onClick (Redirect route)
            ]
            [ text "Modifier" ]
        ]


viewInfoSection : PersonalInfo.Model -> Bool -> Html Msg
viewInfoSection info isValid =
    div []
        [ viewTitle "État civil" Route.PersonalInfo isValid
        , div [ class "container" ]
            [ viewInfoTable info ]
        ]


viewInfoTable : PersonalInfo.Model -> Html Msg
viewInfoTable info =
    viewSimpleTable
        [ ( "Prénom", info.surname )
        , ( "Nom", info.name )
        , ( "Majorité révolue"
          , if info.major then
                "Oui"

            else
                "Non"
          )
        , ( "Adulte responsable", info.responsible )
        ]


viewContactSection : Contact.Model -> Bool -> Html Msg
viewContactSection contact isValid =
    div []
        [ viewTitle "Contact" Route.Contact isValid
        , div [ class "container" ]
            [ viewContactTable contact ]
        ]


viewContactTable : Contact.Model -> Html msg
viewContactTable contact =
    let
        addr =
            contact.address
    in
    viewSimpleTable
        [ ( "Intitulé", addr.line1 )
        , ( "Complément", addr.line2 )
        , ( "Code postal", addr.postalCode )
        , ( "Ville", addr.city )
        , ( "Pays", addr.country )
        , ( "Téléphone", contact.phone )
        , ( "Courriel", contact.email )
        ]


viewBasketSection : Tickets.Model -> Bool -> Html Msg
viewBasketSection tickets isValid =
    div []
        [ viewTitle "Panier" Route.Tickets isValid
        , div [ class "container" ]
            [ viewBasketTable tickets ]
        ]


viewBasketTable : Tickets.Model -> Html msg
viewBasketTable tickets =
    let
        isReduced =
            Maybe.withDefault False tickets.isReduced

        selection =
            Tickets.selection tickets
    in
    case List.length selection of
        0 ->
            p [ class "notification" ]
                [ text "Vous n’avez choisi aucun forfait !" ]

        _ ->
            viewTicketTable selection isReduced



-- TODO: Change the whole Tickets Model and remove this function


viewTicketTable : List ( Ticket, Int ) -> Bool -> Html msg
viewTicketTable tickets isReduced =
    let
        rows =
            tickets
                |> List.sortWith
                    (\( a, _ ) ( b, _ ) ->
                        case Tickets.compareTickets a b of
                            GT ->
                                LT

                            EQ ->
                                EQ

                            LT ->
                                GT
                    )
                |> List.map (viewTicketRow isReduced)

        priceGetter =
            if isReduced then
                .reducedPrice

            else
                .price

        totalPrice =
            tickets
                |> List.map (\( ticket, quantity ) -> priceGetter ticket * toFloat quantity)
                |> List.foldl (+) 0
    in
    table [ class "table is-hoverable is-fullwidth" ]
        [ thead []
            [ th [ style "min-width" "8rem" ] [ text "Article" ]
            , th [ style "min-width" "8rem" ] [ text "Description" ]
            , th [ style "min-width" "8rem" ] [ text "Quantité" ]
            , th [ style "min-width" "8rem" ] [ text "Prix unitaire" ]
            , th [ style "min-width" "8rem" ] [ text "Prix" ]
            ]
        , tbody [] rows
        , tfoot []
            [ th [] [ text "Total" ]
            , th [] []
            , th [] []
            , th [] []
            , th [] [ text <| String.fromFloat totalPrice ++ " €" ]
            ]
        ]


viewTicketRow : Bool -> ( Ticket, Int ) -> Html msg
viewTicketRow isReduced ( ticket, quantity ) =
    let
        unitPrice =
            if isReduced then
                ticket.reducedPrice

            else
                ticket.price

        price =
            unitPrice * toFloat quantity
    in
    tr []
        [ td [] [ text ticket.name ]
        , td [] [ text <| Maybe.withDefault "" ticket.description ]
        , td [] [ text (String.fromInt quantity) ]
        , td [] [ text <| String.fromFloat unitPrice ++ " €" ]
        , td [] [ text <| String.fromFloat price ++ " €" ]
        ]


viewWorkshopsSection : Workshops.Model -> Html Msg
viewWorkshopsSection workshops =
    div [ classList [ ( "is-hidden", Workshops.getTotal workshops <= 0 ) ] ]
        [ viewTitle "Ateliers" Route.Workshops True
        , div [ class "container" ]
            [ viewWorkshopsTable workshops ]
        ]


viewWorkshopsTable : Workshops.Model -> Html msg
viewWorkshopsTable workshops =
    let
        rows =
            Workshops.map viewWorkshopRow workshops
    in
    table [ class "table is-hoverable is-fullwidth" ]
        [ thead []
            [ th [ style "min-width" "8rem" ] [ text "Créneau" ]
            , th [ style "min-width" "8rem" ] [ text "Nom" ]
            , th [ style "min-width" "8rem" ] [ text "Groupe" ]
            ]
        , tbody [] rows
        ]


viewWorkshopRow : ( Day, Moment, Workshop ) -> Html msg
viewWorkshopRow ( day, moment, workshop ) =
    let
        slot =
            Day.toString day ++ " " ++ Moment.toString moment
    in
    tr []
        [ td [] [ text slot ]
        , td [] [ text workshop.name ]
        , td [] [ text workshop.band ]
        ]


viewSimpleTable : List ( String, String ) -> Html msg
viewSimpleTable tuples =
    let
        rows =
            List.map
                (\( s, t ) -> tr [] [ th [ class "has-text-right" ] [ text s ], td [] [ text t ] ])
                tuples
    in
    table [ class "table is-hoverable" ]
        [ tbody [] rows ]



-- UPDATE --


type Msg
    = Next
    | Previous
    | Redirect Route.Route


update : Navigation.Key -> Msg -> Model -> Cmd Msg
update key msg model =
    case msg of
        Previous ->
            let
                previousRoute =
                    if Tickets.workshopsCount model.basket > 0 then
                        Route.Workshops

                    else
                        Route.Tickets
            in
            Route.newUrl key previousRoute

        Next ->
            Route.newUrl key Route.PaymentChoice

        Redirect route ->
            Route.newUrl key route



-- VALIDATE --


validate : Model -> List Section
validate model =
    [ ( PersonalInfo, PersonalInfo.validate model.info |> Dict.isEmpty )
    , ( Contact, Contact.validate model.contact |> Dict.isEmpty )
    , ( Basket, Tickets.validate model.basket |> (\x -> x == Nothing) )
    ]
        |> List.filterMap
            (\( section, isValid ) ->
                if isValid then
                    Nothing

                else
                    Just section
            )
