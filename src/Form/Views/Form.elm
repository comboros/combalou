module Form.Views.Form exposing (inputText)

import Accessibility exposing (Html, div, label, p, text)
import Accessibility.Aria exposing (errorMessage)
import Accessibility.Widget exposing (invalid)
import Html.Attributes exposing (class, classList, for, id, placeholder, type_, value)
import Html.Events exposing (onInput)


type alias InputText msg =
    { label : String
    , placeholder : String
    , onInput : String -> msg
    , value : String
    , error : Maybe String
    , mandatory : Bool
    , id : String
    }


inputText : InputText msg -> Html msg
inputText spec =
    let
        style =
            Maybe.map (\x -> "input is-danger") spec.error
                |> Maybe.withDefault "input"
    in
    div [ class "field" ]
        [ label
            [ class "label"
            , classList [ ( "mandatory", spec.mandatory ) ]
            , for spec.id
            ]
            [ text spec.label ]
        , div [ class "control" ]
            [ Accessibility.inputText
                spec.value
                [ class style
                , placeholder spec.placeholder
                , onInput spec.onInput
                , id spec.id
                , invalid (spec.error /= Nothing)
                , errorMessage (spec.id ++ "-error")
                ]
            ]
        , p [ class "help is-danger", id (spec.id ++ "-error") ]
            (Maybe.map (List.singleton << text) spec.error
                |> Maybe.withDefault []
            )
        ]



{-
   viewError : FV.FieldError () -> Html msg
   viewError error =
       let
           message =
               case error of
                   FV.MissingField ->
                       "Ce champ est requis"

                   FV.WrongType ->
                       "Mauvais type"

                   FV.NotInt ->
                       "Ceci n’est pas un nombre"

                   FV.NotFloat ->
                       "Ceci n’est pas un nombre"

                   FV.EmptyString ->
                       "Ce champ est requis"

                   FV.WrongLength ->
                       "Taille invalide"

                   FV.NotEmail ->
                       "Ce courriel ne semble pas valide"

                   FV.PasswordNotEqual ->
                       "Les mots de passe ne sont pas égaux"

                   FV.CustomError _ ->
                       "Erreur inconnue"
       in
           p [ class "help is-danger" ] [ text message ]
-}
