module Form.Main exposing (main)

import Accessibility as Html exposing (Html, a, article, div, h1, h2, p, section, text)
import Accessibility.Landmark exposing (banner)
import Browser
import Browser.Dom as Dom
import Browser.Navigation as Navigation
import Common.Url as Url
import Common.Workshop
import Form.Data.Catalog
import Form.Data.Workshops
import Form.Pages.Common as Common
import Form.Pages.Contact as Contact
import Form.Pages.Home as Home
import Form.Pages.PaymentChoice as PaymentChoice
import Form.Pages.PersonalInfo as PersonalInfo
import Form.Pages.RegistrationResult as RegistrationResult
import Form.Pages.Summary as Summary
import Form.Pages.Tickets as Tickets
import Form.Pages.Workshops as Workshops
import Form.Route as Route exposing (..)
import Html.Attributes exposing (class)
import Http
import Json.Decode exposing (Value, int)
import Json.Encode as Json
import RemoteData exposing (RemoteData(..), WebData)
import Task
import Url exposing (Url)



-- MAIN --


main : Program () Model Msg
main =
    Browser.application
        { init = init
        , view = \model -> { title = "Comboros 2019 — Inscriptions", body = [ view model ] }
        , update = update
        , subscriptions = subscriptions
        , onUrlChange = ChangedUrl
        , onUrlRequest = ClickedLink
        }


init : () -> Url.Url -> Navigation.Key -> ( Model, Cmd Msg )
init flags url key =
    setRoute (Route.fromUrl url)
        { key = key
        , page = Home
        , info = PersonalInfo.init
        , contact = Contact.init
        , basket = NotAsked
        , workshops = NotAsked
        , paymentChoice = PaymentChoice.init
        , registrationId = NotAsked
        }



-- MODEL --


type alias Model =
    { key : Navigation.Key
    , page : Page
    , info : PersonalInfo.Model
    , contact : Contact.Model
    , basket : WebData Tickets.Model
    , workshops : WebData Workshops.Model
    , paymentChoice : PaymentChoice.Model
    , registrationId : WebData RegistrationId
    }


type Page
    = Home
    | PersonalInfo
    | Contact
    | Tickets
    | Workshops
    | Summary
    | PaymentChoice
    | RegistrationResult


type RegistrationId
    = RegistrationId Int


encode : Model -> Json.Value
encode model =
    case model.basket of
        Success basket ->
            let
                workshops =
                    RemoteData.withDefault Form.Data.Workshops.empty model.workshops

                priceType =
                    basket.isReduced
                        |> Maybe.withDefault False
                        |> (\x ->
                                if x then
                                    "reduced"

                                else
                                    "full"
                           )
                        |> Json.string
            in
            Json.object
                [ ( "personal-info", PersonalInfo.encode model.info )
                , ( "contact", Contact.encode model.contact )
                , ( "basket", Tickets.encode basket )
                , ( "workshops", Workshops.encode workshops )
                , ( "payment-mode", PaymentChoice.encode model.paymentChoice )
                , ( "price-type", priceType )
                ]

        _ ->
            Json.object []



-- UPDATE --


type Msg
    = NoOp
    | ChangedUrl Url
    | ClickedLink Browser.UrlRequest
    | HomeMsg Home.Msg
    | PersonalInfoMsg PersonalInfo.Msg
    | ContactMsg Contact.Msg
    | TicketsMsg Tickets.Msg
    | CatalogLoaded (WebData Form.Data.Catalog.Catalog)
    | WorkshopsMsg Workshops.Msg
    | WorkshopsLoaded (Result Http.Error Workshops.Model)
    | SummaryMsg Summary.Msg
    | PaymentChoiceMsg PaymentChoice.Msg
    | Retry (Cmd Msg)
    | RegistrationResponse (WebData Int)
    | FocusMain (Result Dom.Error ())


setRoute : Maybe Route -> Model -> ( Model, Cmd Msg )
setRoute route model =
    case model.registrationId of
        NotAsked ->
            case route of
                Nothing ->
                    -- TODO load 404 page not found :(
                    ( model, focusToMain )

                Just Route.Home ->
                    ( { model | page = Home }, focusToMain )

                Just Route.PersonalInfo ->
                    ( { model | page = PersonalInfo }, focusToMain )

                Just Route.Contact ->
                    ( { model | page = Contact }, focusToMain )

                Just Route.Tickets ->
                    case model.basket of
                        NotAsked ->
                            ( { model
                                | page = Tickets
                                , basket = Loading
                              }
                            , fetchCatalog
                            )

                        _ ->
                            ( { model | page = Tickets }, focusToMain )

                Just Route.Workshops ->
                    case model.workshops of
                        NotAsked ->
                            ( { model
                                | page = Workshops
                                , workshops = Loading
                              }
                            , fetchWorkshops
                            )

                        _ ->
                            ( { model | page = Workshops }, focusToMain )

                Just Route.Summary ->
                    ( { model | page = Summary }, focusToMain )

                Just Route.PaymentChoice ->
                    ( { model | page = PaymentChoice }, focusToMain )

                Just Route.RegistrationResult ->
                    ( { model | page = RegistrationResult }
                    , Cmd.batch [ saveRegistration model, focusToMain ]
                    )

        _ ->
            ( { model | page = RegistrationResult }, focusToMain )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( msg, model.page ) of
        ( NoOp, _ ) ->
            ( model, Cmd.none )

        ( ClickedLink urlRequest, _ ) ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Navigation.pushUrl model.key (Url.toString url) )

                Browser.External url ->
                    ( model, Navigation.load url )

        ( ChangedUrl url, _ ) ->
            setRoute (Route.fromUrl url) model

        ( HomeMsg subMsg, Home ) ->
            -- No model at all
            ( model
            , Home.update model.key subMsg
                |> Cmd.map HomeMsg
            )

        ( PersonalInfoMsg subMsg, PersonalInfo ) ->
            let
                ( newModel, newCmd ) =
                    PersonalInfo.update model.key subMsg model.info
            in
            ( { model | info = newModel }, Cmd.map PersonalInfoMsg newCmd )

        ( ContactMsg subMsg, Contact ) ->
            let
                ( newModel, newCmd ) =
                    Contact.update model.key subMsg model.contact
            in
            ( { model | contact = newModel }, Cmd.map ContactMsg newCmd )

        ( TicketsMsg subMsg, Tickets ) ->
            let
                ( newModel, newCmd ) =
                    RemoteData.update (Tickets.update model.key subMsg) model.basket
            in
            ( { model | basket = newModel }, Cmd.map TicketsMsg newCmd )

        ( CatalogLoaded response, _ ) ->
            ( { model | basket = RemoteData.map Tickets.init response }, Cmd.none )

        ( WorkshopsMsg subMsg, Workshops ) ->
            let
                ( newModel, newCmd ) =
                    RemoteData.update (Workshops.update model.key subMsg) model.workshops
            in
            ( { model | workshops = newModel }, Cmd.map WorkshopsMsg newCmd )

        ( WorkshopsLoaded (Ok workshops), _ ) ->
            ( { model | workshops = Success workshops }, Cmd.none )

        ( WorkshopsLoaded (Err error), _ ) ->
            ( { model | workshops = Failure error }, Cmd.none )

        ( SummaryMsg subMsg, Summary ) ->
            case model.basket of
                Success basket ->
                    let
                        workshops =
                            RemoteData.withDefault Form.Data.Workshops.empty model.workshops
                    in
                    summaryModel model basket workshops
                        |> Summary.update model.key subMsg
                        |> Cmd.map SummaryMsg
                        |> (\x -> ( model, x ))

                _ ->
                    ( model, Cmd.none )

        ( PaymentChoiceMsg subMsg, PaymentChoice ) ->
            let
                ( newModel, newCmd ) =
                    PaymentChoice.update model.key subMsg model.paymentChoice
            in
            ( { model | paymentChoice = newModel }
            , Cmd.map PaymentChoiceMsg newCmd
            )

        ( RegistrationResponse id, _ ) ->
            ( { model
                | registrationId = RemoteData.map RegistrationId id
              }
            , Cmd.none
            )

        ( _, _ ) ->
            -- wrong messages are ignored
            ( model, Cmd.none )


summaryModel : Model -> Tickets.Model -> Workshops.Model -> Summary.Model
summaryModel model basket workshops =
    { info = model.info
    , contact = model.contact
    , basket = basket
    , workshops = workshops
    }



-- VIEW --


viewPage : Model -> Html Msg
viewPage model =
    case model.page of
        Home ->
            Home.view |> Html.map HomeMsg

        PersonalInfo ->
            PersonalInfo.view model.info
                |> Html.map PersonalInfoMsg

        Contact ->
            Contact.view model.contact
                |> Html.map ContactMsg

        Tickets ->
            case model.basket of
                Success basket ->
                    Tickets.view basket |> Html.map TicketsMsg

                Failure err ->
                    Common.viewFetchError err (Retry fetchCatalog)

                _ ->
                    Common.viewSpinner

        Workshops ->
            case RemoteData.append model.basket model.workshops of
                Success ( basket, workshops ) ->
                    let
                        count =
                            Tickets.workshopsCount basket
                    in
                    Workshops.view count workshops
                        |> Html.map WorkshopsMsg

                Failure err ->
                    Common.viewFetchError err (Retry fetchWorkshops)

                _ ->
                    Common.viewSpinner

        Summary ->
            case model.basket of
                Success basket ->
                    let
                        workshops =
                            RemoteData.withDefault Form.Data.Workshops.empty model.workshops
                    in
                    summaryModel model basket workshops
                        |> Summary.view
                        |> Html.map SummaryMsg

                Failure err ->
                    Common.viewFetchError err NoOp

                _ ->
                    Common.viewSpinner

        PaymentChoice ->
            case model.basket of
                Success basket ->
                    PaymentChoice.view model.paymentChoice (Tickets.total basket)
                        |> Html.map PaymentChoiceMsg

                Failure err ->
                    Common.viewFetchError err (Retry fetchCatalog)

                _ ->
                    viewLogicError "Cette page n’est pas accessible avant d’avoir complété le formulaire !"

        RegistrationResult ->
            case RemoteData.append model.basket model.registrationId of
                Success ( basket, RegistrationId id ) ->
                    let
                        amount =
                            Tickets.total basket

                        registrationLabel =
                            "Comboros #"
                                ++ String.fromInt id
                                ++ " "
                                ++ model.info.surname
                                ++ " "
                                ++ model.info.name
                    in
                    RegistrationResult.view <|
                        RegistrationResult.Model model.paymentChoice
                            amount
                            registrationLabel

                Loading ->
                    Common.viewSpinner

                NotAsked ->
                    Common.viewSpinner

                _ ->
                    viewLogicError "Cette page n’est pas accessible avant d’avoir complété le formulaire !"


view : Model -> Html Msg
view model =
    div []
        [ section [ banner, class "hero is-primary" ]
            [ div [ class "hero-body" ]
                [ div [ class "container" ]
                    [ h1 [ class "title is-1" ]
                        -- TODO http://www.comboros.com/assets/images/interface/logo-blanc.png
                        [ text "Bienvenue à Comboros !" ]
                    , h2 [ class "subtitle is-2" ]
                        [ text "Inscription à l’édition 2019" ]
                    ]
                ]
            ]
        , viewPage model
        ]


viewLogicError : String -> Html msg
viewLogicError reason =
    article [ class "section message is-danger" ]
        [ div [ class "message-header" ]
            [ p []
                [ text "Il s’est passé quelque chose d’étrange…" ]
            ]
        , div [ class "message-body" ]
            [ p []
                [ text reason ]
            , p []
                [ text "Vous pourriez "
                , a [ Route.href Route.Home ] [ text "revenir à l’accueil" ]
                , text "."
                ]
            ]
        ]



-- HTTP --


fetchCatalog : Cmd Msg
fetchCatalog =
    RemoteData.sendRequest Form.Data.Catalog.request
        |> Cmd.map CatalogLoaded


fetchWorkshops : Cmd Msg
fetchWorkshops =
    Common.Workshop.fetchAll
        |> Http.send (WorkshopsLoaded << Result.map Form.Data.Workshops.fromList)


saveRegistration : Model -> Cmd Msg
saveRegistration model =
    let
        body =
            encode model
                |> Http.jsonBody
    in
    Http.post Url.registrations body int
        |> RemoteData.sendRequest
        |> Cmd.map RegistrationResponse


{-| Focus to the main\_ section
-}
focusToMain : Cmd Msg
focusToMain =
    Task.attempt FocusMain (Dom.focus "main-section")



-- SUBSCRIPTIONS --


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none
