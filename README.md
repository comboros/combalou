# Installation
## Frontend

1. [Installer NodeJS](https://nodejs.org/).
2. Cloner le dépôt Git : `git clone https://framagit.org/rbd/comboros-form-2018`.
3. Télécharger et installer les dépendances : dans le dossier `comboros-form-2018`, exécuter `npm run reinstall` (ça peut prendre un petit moment).
4. Démarrer l’appli : toujours dans le dossier `comboros-form-2018`, exécuter `npm start`.
5. Naviguer sur [localhost:8080](http://localhost:8080).

## Backend

Prérequis : MariaDB/MySQL et Php.

1. Côté MySQL, créer une base vide et un utilisateur ayant les droits dessus (y compris les droits FILE).
2. Dans le dossier `migrations`, exécuter la commande `./run-script.sh <script>` sur les fichiers `init.sql`, `catalog.sql`, `workshops.sql` dans cet ordre.
3. Démarrer le serveur : exécuter `./run-server.sh` dans la racine du projet.
