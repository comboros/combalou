LOAD DATA INFILE 'catalog.csv'
  INTO TABLE ${pfx}catalog
  FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
  LINES TERMINATED BY '\n'
  IGNORE 1 LINES
  (span, name, price, reduced_price, description, workshops);


