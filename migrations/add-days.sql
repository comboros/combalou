-- Update the `day` column to have all possible days

ALTER TABLE ${pfx}workshops
MODIFY COLUMN day ENUM('saturday', 'sunday', 'monday', 'tuesday', 'thursday', 'wednesday', 'friday')
NOT NULL;

