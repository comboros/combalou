LOAD DATA INFILE 'workshops.csv'
  INTO TABLE ${pfx}workshops
  FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
  LINES TERMINATED BY '\n'
  IGNORE 1 LINES
  (name, band, category, day, moment, gauge);

