-- ${pfx} is to replace with you current table prefix (`ct_`, for instance)
-- You can use sed for that:
-- sed 's/\${pfx}/ct_/g' init.sql

-- TABLES --

CREATE TABLE ${pfx}register (
  id INTEGER NOT NULL AUTO_INCREMENT,
  price_type ENUM('full', 'reduced') NOT NULL,
  payment_type ENUM('card', 'check', 'transfer') NOT NULL,
  state ENUM('unpaid', 'paid', 'inconsistent') NOT NULL,


  -- personal info
  surname VARCHAR(255) NOT NULL,
  name VARCHAR(255) NOT NULL,
  responsible VARCHAR(255), -- if null, person is major

  -- address
  line1 VARCHAR(255) NOT NULL,
  line2 VARCHAR(255),
  postal_code VARCHAR(30) NOT NULL,
  city VARCHAR(255) NOT NULL,
  country VARCHAR(255) NOT NULL,
  phone VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL,

  PRIMARY KEY (id)
);

CREATE TABLE ${pfx}catalog (
  id INTEGER NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  description TEXT,
  price DECIMAL(8,2) NOT NULL,
  reduced_price DECIMAL(8,2) NOT NULL,
  span ENUM('weekly', 'daily', 'unitary') NOT NULL,
  workshops INTEGER NOT NULL,

  PRIMARY KEY (id)
);

CREATE TABLE ${pfx}workshops (
  id INTEGER NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  band VARCHAR(255) NOT NULL,
  day ENUM('saturday', 'sunday', 'monday', 'tuesday') NOT NULL,
  moment ENUM('morning', 'afternoon') NOT NULL,
  category ENUM('danse', 'instrument', 'autre') NOT NULL,
  gauge INTEGER NOT NULL,

  PRIMARY KEY (id)
);

CREATE TABLE ${pfx}basket (
  registration INTEGER NOT NULL,
  ticket INTEGER NOT NULL,
  quantity INTEGER NOT NULL,

  PRIMARY KEY (registration, ticket)
);

CREATE TABLE ${pfx}booking (
  registration INTEGER NOT NULL,
  workshop INTEGER NOT NULL,

  PRIMARY KEY (registration, workshop)
);

-- CONSTRAINTS --

ALTER TABLE ${pfx}basket
  ADD CONSTRAINT ${pfx}_fk_basket_registration
  FOREIGN KEY (registration)
  REFERENCES ${pfx}register (id)
  ON DELETE CASCADE;

ALTER TABLE ${pfx}basket
  ADD CONSTRAINT ${pfx}_fk_basket_ticket
  FOREIGN KEY (ticket)
  REFERENCES ${pfx}catalog (id)
  ON DELETE CASCADE;

ALTER TABLE ${pfx}booking
  ADD CONSTRAINT ${pfx}_fk_booking_registration
  FOREIGN KEY (registration)
  REFERENCES ${pfx}register (id)
  ON DELETE CASCADE;

ALTER TABLE ${pfx}booking
  ADD CONSTRAINT ${pfx}_fk_booking_workshop
  FOREIGN KEY (workshop)
  REFERENCES ${pfx}workshops (id)
  ON DELETE CASCADE;

