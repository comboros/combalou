module MaybeSelectListTests exposing (..)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, intRange, list, string)
import Test exposing (..)
import Common.MaybeSelectList exposing (..)


-- FUZZERS --
-- TESTS --


afterTests : Test
afterTests =
    describe "after"
        [ fuzz (list int) "must return an empty list when nothing is selected" <|
            \lst ->
                after (fromList lst)
                    |> Expect.equal []
        ]


beforeTests : Test
beforeTests =
    describe "before"
        [ fuzz (list int) "must return the whole list when nothing is selected" <|
            \lst ->
                before (fromList lst)
                    |> Expect.equal lst
        ]


lengthTests : Test
lengthTests =
    describe "length"
        [ fuzz (list int) "must be preserved by fromList" <|
            \lst ->
                length (fromList lst)
                    |> Expect.equal (List.length lst)
        , test "of an empty list must be 0" <|
            \_ ->
                length (fromList [])
                    |> Expect.equal 0
        ]


toListTests : Test
toListTests =
    describe "toList"
        [ fuzz (list int) "must reverse fromList" <|
            \lst ->
                fromList lst
                    |> toList
                    |> Expect.equal lst
        , test "must map an empty MaybeSelectList to an empty list" <|
            \_ ->
                fromList []
                    |> toList
                    |> Expect.equal []
        ]


selectTests : Test
selectTests =
    describe "select"
        [ test "must be identity if the new selection is not in the list" <|
            \_ ->
                let
                    lst =
                        fromList [ 1, 2, 3 ]
                in
                    select 4 lst
                        |> Expect.equal lst
        , fuzz2 (list (intRange 0 20)) (intRange 0 20) "must be idempotent" <|
            \l n ->
                let
                    lst =
                        fromList l
                in
                    lst
                        |> select n
                        |> select n
                        |> Expect.equal (select n lst)
        , fuzz2 (list (intRange 0 20)) (intRange 0 20) "must preserve length" <|
            \lst n ->
                fromList lst
                    |> select n
                    |> length
                    |> Expect.equal (List.length lst)
        , fuzz2 (list (intRange 0 20)) (intRange 0 20) "must preserve the underlying list" <|
            \lst n ->
                fromList lst
                    |> select n
                    |> toList
                    |> Expect.equal lst
        , test "called twice must preserve the underlying list" <|
            \_ ->
                fromList [ 1, 2, 3, 4, 5, 6 ]
                    |> select 1
                    |> select 6
                    |> toList
                    |> Expect.equal [ 1, 2, 3, 4, 5, 6 ]
        ]


unselectTests =
    describe "unselect"
        [ fuzz (list int) "must be identity if nothing is selected" <|
            (\l ->
                let
                    lst =
                        fromList l
                in
                    unselect lst
                        |> Expect.equal lst
            )
        , fuzz2 (list (intRange 0 20)) (intRange 0 20) "must preserve length" <|
            (\l n ->
                fromList l
                    |> select n
                    |> unselect
                    |> length
                    |> Expect.equal (List.length l)
            )
        , fuzz2 (list (intRange 0 20)) (intRange 0 20) "must preserve underlying list" <|
            (\l n ->
                fromList l
                    |> select n
                    |> unselect
                    |> toList
                    |> Expect.equal l
            )
        ]
